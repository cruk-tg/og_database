module ActiveRecord::ConnectionAdapters
  class JdbcAdapter < AbstractAdapter
    def explain(query, *binds)
      ActiveRecord::Base.connection.execute("EXPLAIN QUERY PLAN #{query}", 'EXPLAIN', binds)
    end
  end
end

class CasProxyCallbackController < ActionController::Base
  def open_pstore
    PStore.new("#{::Rails.root}/tmp/cas_pgt.pstore")
  end
end
