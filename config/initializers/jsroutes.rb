JsRoutes.setup do |config|
  if Rails.env.production?
    config.prefix = "https://www.ecmc.ed.ac.uk/#{ENV['RAILS_RELATIVE_URL_ROOT'].sub(/^\//,'')}"
  end
end
