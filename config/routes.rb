OgDatabase::Application.routes.draw do
  get 'definitions/show'

  resources :demographics, :shallow=>true do
    collection do
      get 'search'
      post 'search'
      get 'recurrences'
      get 'reports'
      post 'reports'
    end
    resources :follow_ups
    resources :pathologies
    resources :treatments
    resources :staging_cts
    resources :stagings
    resources :exposures
    resources :samples
    resources :ffpes
  end

  get 'logout', to: 'demographics#logout'
  root :to => 'demographics#search'
end
