require 'torquebox-capistrano-support'
require 'bundler/capistrano'

set :application, "og_database"

set :scm, :bzr
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
set :deploy_to,         "/opt/torquebox/apps"
set :torquebox_home,    '/opt/torquebox/torquebox-current'
set :rails_env,         "development"
set :app_context,       "/og_database"
set :repository, "bzr+ssh://svn.prv.ecmc.ed.ac.uk/var/lib/bzr/og_database/head/"
set :deploy_via, :copy
#set :copy_strategy, :export
#set :copy_cache, true

#default_run_options[:pty] = true

role :web, "www.ecmc.ed.ac.uk"                          # Your HTTP server, Apache/etc
role :app, "tb2-01.prv.ecmc.ed.ac.uk"                          # This may be the same as your `Web` server
role :db,  "ecmc-mysql02.prv.ecmc.ed.ac.uk", :primary => true # This is where Rails migrations will run

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end
