# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = '2'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Use Ubuntu 14.04 Trusty Tahr 64-bit as our operating system
  config.vm.box = 'ubuntu/xenial64'
  config.ssh.forward_agent = true
  config.ssh.forward_x11   = true
  # Configurate the virtual machine to use 2GB of RAM
  config.vm.provider :virtualbox do |vb|
    vb.customize ['modifyvm', :id, '--memory', '2048']
  end

  # Forward the Rails server default port to the host
  config.vm.network 'forwarded_port', guest: 3000, host: 3000

  config.vm.provision "file", source: "~/.gitconfig", destination: ".gitconfig"

  # Use Chef Solo to provision our virtual machine
  config.vm.provision :chef_zero do |chef|
    chef.cookbooks_path = 'cookbooks'
    chef.nodes_path = 'nodes'
    chef.add_recipe 'locale'
    chef.add_recipe 'system'
    chef.add_recipe 'apt'
    chef.add_recipe 'firewall'

    chef.add_recipe 'ecmc_docker'
    chef.add_recipe 'ecmc_mysql::mysql_5_7'
    chef.add_recipe 'ecmc_development_setup'
    chef.add_recipe 'ecmc_development_setup::ruby_install'
    chef.add_recipe 'ecmc_development_setup::node_install'
    chef.add_recipe 'phantom_js'

    # Install Ruby 2.3.0 and Bundler
    # Set an empty root password for MySQL to make things simple
    chef.json = {
      locale: {
        lang: 'en_GB.UTF-8'
      },
      system: {
        timezone: 'Europe/London'
      }
    }
  end
end
