source 'https://rubygems.org'

gem 'rails', '~> 5.1'
gem 'activemodel-serializers-xml'

gem 'audited'
gem 'axlsx_rails'
gem 'cocoon'
gem 'draper'
gem 'formtastic'
gem 'haml'
gem 'rack-cas'
gem 'rack-heartbeat'

# Gems used only for assets and not required
# in production environments by default.
gem 'hiredis'
gem 'redis', :require => ["redis", "redis/connection/hiredis"]

gem 'sass-rails'
gem 'coffee-rails'
gem 'js-routes'
gem 'therubyracer', :platforms => :ruby
gem 'uglifier'
gem 'mysql2'

# Logging
gem 'exception_notification'
gem 'lograge'
gem 'logstash-event'
gem 'logstash-logger'
gem 'slack-notifier'

source 'https://rails-assets.org' do
  gem 'rails-assets-jquery'
  gem 'rails-assets-jquery-ujs'
  gem 'rails-assets-bootstrap'
  gem 'rails-assets-momentjs'
  gem 'rails-assets-datatables.net'
  gem 'rails-assets-datatables.net-bs'
  gem 'rails-assets-log4javascript'
  gem 'rails-assets-notify'
  gem 'rails-assets-sugar'
  gem 'rails-assets-urijs'
  gem 'rails-assets-bootbox'
  gem 'rails-assets-fontawesome'
  gem 'rails-assets-notifyjs'
  gem 'rails-assets-eonasdan-bootstrap-datetimepicker'
  gem 'rails-assets-autosize'
end

group :development do
  gem 'web-console'
end

group :test, :development do
  gem 'byebug'
end

group :test do
  gem 'cucumber'
  gem 'cucumber-rails', :require => false
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'launchy'
  gem 'lorem'
  gem 'mocha', :require => false
  gem 'poltergeist'
  gem 'rspec'
  gem 'rspec-rails'
  gem 'shoulda-matchers'
  gem 'rails-controller-testing'
end
