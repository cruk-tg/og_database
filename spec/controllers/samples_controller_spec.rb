require 'spec_helper'

RSpec.describe SamplesController, type: :controller do
  let (:participant_label) { "OE000007" }

  before(:each) do
    @demographic = FactoryBot.create(:demographic, :oe_number => participant_label)
    @sample_with_location = FactoryBot.create(:sample, :demographic_id => @demographic.id, :label => participant_label.concat('A'), :extra => {:storage_location => 'A Box'})
    @sample_without_location = FactoryBot.create(:sample, :demographic_id => @demographic.id, :label => participant_label.concat('A'), :extra => {:storage_location => nil})

  end

  describe "GET 'index'" do
    it "returns http success" do
      get :index, params: {demographic_id:  @demographic.id}
      response.should be_success
    end

    it "assigns demographic" do
      get :index, params: {demographic_id:  @demographic.id}
      assigns(:demographic).should eq(@demographic)
    end

    it "assigns samples from database with a location" do
      get :index, params: {demographic_id:  @demographic.id}
      assigns(:samples).should include(@sample_with_location)
    end

    it "does not assign samples from the database without a location" do
      get :index, params: {demographic_id:  @demographic.id}
      assigns(:samples).should_not include(@sample_without_location)
    end
  end

  describe "GET 'edit'" do
    before(:each) do
      @sample = FactoryBot.create(:sample)
    end

    it "returns http success" do
      get :edit, params: {id: @sample.id}
      response.should be_success
    end
  end
end
