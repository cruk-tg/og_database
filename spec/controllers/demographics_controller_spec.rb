require 'spec_helper'

RSpec.describe DemographicsController, type: :controller do
  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "GET 'search'" do
    it "return http success" do
      get 'search'
      response.should be_success
    end
  end

  describe "POST search_results" do
    context "by treatment phase" do
      before(:each) do
        @demographic = FactoryBot.create(:demographic)
        @demographic_postop = FactoryBot.create(:demographic_postop)
        @demographic_recurrence_free = FactoryBot.create(:demographic_recurrence_free)
      end

      it "post-operativly" do
        post :search, params: {:quickq => "navbar-post-op"}
        assigns(:search_results).should include(@demographic_postop)
      end

      it "pre-operativly" do
        post :search, params: {:quickq => "navbar-pre-op"}
        assigns(:search_results).should include(@demographic)
      end

      it "after op but without pathology results" do
        post :search, params: {:quickq => "navbar-recurrence-free"}
        assigns(:search_results).should include(@demographic_recurrence_free)
      end

      it "after op but without pathology number" do
        bug = FactoryBot.create :demographic_pathology_absent_bug
        post :search, params: {:quickq => "navbar-pathology-absent"}
        assigns(:search_results).should include(bug)
      end
    end
  end

  describe "POST create" do
    context "valid demographic" do
      it "create a new demographic" do
        expect {
          demographic = FactoryBot.build(:demographic).attributes.delete_if { |k,v| ["id","created_at", "updated_at"].include?(k) }
          post :create, params: {demographic: demographic}
        }.to change(Demographic,:count).by(1)
      end
    end
  end
  describe "PUT update'" do
    before(:each) do
      @demographic = FactoryBot.create(:demographic)
    end

    context "valid demographic" do
      it "update a demographic" do
        params = { id: @demographic, demographic: FactoryBot.attributes_for(:demographic) }
        put :update, params: params
        assigns(:demographic).should eq(@demographic)
      end
    end
  end
end
