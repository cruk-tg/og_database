# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :post_chemotherapy_restaging do
    pcr_type_id 1
    findings_id 1
    demographic_id 1
    staging_id 1
  end
end
