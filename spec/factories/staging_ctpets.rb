# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :staging_ctpet do
    report "MyText"
    tnm_t_id 1
    tnm_n_id 1
    tnm_m_id 1
    positive_node_location_id 1
    demographic_id 1
  end
end
