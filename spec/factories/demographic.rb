# Read about factories at https://github.com/thoughtbot/factory_girl
FactoryBot.define do

  factory :demographic do
    sequence(:hospital_number) { |n| "500#{'%07d' % n}X" }
    sequence(:chi) { |n| "#{'%010d' % n }" }
    dob "1950-01-01"
    gp_name "MyString"
    gp_address1 "MyString"
    gp_address2 "MyString"
    gp_address3 "MyString"
    gp_address4 "MyString"
    gp_phone_number "MyString"
    date_of_ecmc_consent "2012-01-01"
    date_of_occams_consent "2012-01-01"
    sequence(:occams_id) { |n| "OCCAMS/ED/#{'%03d' % n}" }
    sequence(:oe_number) { |n| "OE#{'%06d' % n}" }

    factory :demographic_recurrence_free do
      after(:create) do |demographic,ev|
        FactoryBot.create(:treatment, {:demographic => demographic, :surgery_date => 1.year.ago, :surgery_happened => "Yes", :post_op_intent => 'Adjuvant'})
      end
    end

    factory :demographic_postop do
      after(:create) do |demographic,ev|
        FactoryBot.create(:treatment, {:demographic => demographic, :surgery_date => 1.year.ago, :surgery_happened => "Yes", :post_op_intent => 'Adjuvant'})
        FactoryBot.create(:follow_up, {:demographic => demographic, :date_of_recurrence => 1.year.ago })
      end
    end

    factory :demographic_pathology_absent_bug do
      after(:create) do |demographic,ev|
        FactoryBot.create(:treatment, {:demographic => demographic, :surgery_happened => 'Yes'} )
        FactoryBot.create(:pathology, {:demographic => demographic, :diagnostic_path_no => '', :resection_pathology_number => '' })
      end
    end
  end
end
