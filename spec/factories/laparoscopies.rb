# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :laparoscopy do
    staging_id 1
    tumour_location_id 1
    dmpd_id 1
    ascites_id 1
    primary_tumour_extension_id 1
    date Date.today
    macroscopic_findings "This report"
  end
end
