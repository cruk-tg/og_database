# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :treatment do
    surgery_happened "Yes"
    surgery_date "2012-08-24"
    surgery_consultant "Yes"
    surgery_operation "Yes"
  end
end
