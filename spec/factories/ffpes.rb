# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :ffpe do
    demographic_id 1
    tma "MyString"
    tma_cores "MyString"
    core_location "MyString"
    path_response_slides 1
    slide_location "MyString"
  end
end
