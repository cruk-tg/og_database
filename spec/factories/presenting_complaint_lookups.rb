# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :presenting_complaint_lookup do
    sequence :name do |n|
      "PCL_#{n}"
    end
  end
end
