# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :final_mdt_stage do
    demographic_id 1
    tnm_t_id 1
    tnm_n_id 1
    tnm_m_id 1
    mets_location_id 1
  end
end
