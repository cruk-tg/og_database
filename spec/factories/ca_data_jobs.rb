# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :ca_data_job do
    started { Time.now }

    trait :successful do
      status :successful
    end

    trait :failed do
      status :failed
    end

    trait :in_progress do
      status :in_progress
    end
  end
end
