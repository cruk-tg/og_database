# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :chemotherapy_regimen do
    name "MyString"
    agents_csv "MyString"
  end
end
