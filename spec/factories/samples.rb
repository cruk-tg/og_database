# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :sample do
    sequence :label do |n|
      "Label_#{n}"
    end
    sequence :barcode do |n|
      "B#{n}"
    end
    extra {{storage_location: 'Box'}}
  end
end
