# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :other_investigation do
    staging_id 1
    report "MyText"
    metastatsis_id 1
  end
end
