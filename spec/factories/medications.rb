# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :medication do
    exposure_id 1
    drug_id 1
    length_of_use_id 1
  end
end
