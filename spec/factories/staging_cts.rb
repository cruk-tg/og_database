# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :staging_ct do
    report "MyString"
    positive_nodes 1
    mets_location_id 1
  end
end
