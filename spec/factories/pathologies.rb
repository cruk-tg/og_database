# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :pathology do
    demographic_id do
      (Demographic.all.first || FactoryBot.create(:demographic)).id
    end

    diagnostic_path_no ""
    maximum_tumour_diameter 1.5
    distance_to_proximal_margin 1.5
    distance_to_distal_margin 1.5
    distance_to_circumferential_resection_margin 1.5
    number_of_nodes 1
    postive_nodes 1
    location_of_involved_nodes "MyString"
  end
end
