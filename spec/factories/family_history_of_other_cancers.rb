# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :family_history_of_other_cancer do
    yes_no_unk_id 1
    relationship_id 1
    exposure_id ""
  end
end
