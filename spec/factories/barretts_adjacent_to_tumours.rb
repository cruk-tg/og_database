# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :barretts_adjacent_to_tumour do
    pathology_id 1
    barretts_adjacent_lookup_id 1
  end
end
