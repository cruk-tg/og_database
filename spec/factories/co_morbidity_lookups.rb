# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :co_morbidity_lookup do
    sequence :name do |n|
      "co_m_#{n}"
    end
  end
end
