# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :exposure do
    demographic_id do
      (Demographic.all.first || FactoryBot.create(:demographic)).id
    end
    barretts_length 1
    prague_classification_c 1
    prague_classification_m 1
    smoker "Yes"
    smoker_packs 1
    smoker_years 1
    weight 95
    height 1.95
  end
end
