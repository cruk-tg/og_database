# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :staging do
    date_of_diagnosis "2012-08-13"
    tumour_location_id 1
    histology_id 1
    demographic_id 1
  end
end
