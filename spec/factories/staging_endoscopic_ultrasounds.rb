# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :staging_endoscopic_ultrasound do
    demographic_id 1
    report "MyText"
    tumour_length_x 1
    tumour_length_y 1
    tumour_length_z 1
    tumour_type_id 1
    tnm_t_id 1
    tnm_n_id 1
    tnm_m_id 1
    mets_location_id 1
    fna_location_id 1
    nodes "MyText"
    tumour_from 1.1
    tumour_to 1.1
  end
end
