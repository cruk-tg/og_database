# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :follow_up do
    association :demographic
    censor_value "Yes"
    dod Date.today
    dls Date.today.yesterday
    date_of_recurrence Date.today
    region_of_recurrence "MyText"
  end
end
