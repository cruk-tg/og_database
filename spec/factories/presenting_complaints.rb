# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :presenting_complaint do
    presenting_complaint_lookup_id do
      (PresentingComplaintLookup.all.first || FactoryBot.create(:presenting_complaint_lookup)).id
    end
  end
end
