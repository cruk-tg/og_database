FactoryBot.define do
  factory :specimen do
    label "MyString"
    barcode "MyString"
    sample_class "MyString"
    sample_type "MyString"
    collection_time Time.now
    frozen_time Time.now + 45.minutes
    haemolysed "MyString"
    event_position "MyString"
    location "MyString"
    section_undertaken "MyString"
    histology "MyString"
    path_response_slides "MyString"
    histology_text "MyString"
    on_tma "MyString"
    tma_cores "MyString"
    core_location "MyString"
    demographic_id 0
  end
end
