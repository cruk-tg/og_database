require 'spec_helper'

describe DemographicDecorator do
  let(:decorated) { Demographic.new.decorate }
  context "#fields" do
    it "should have occams_id" do
      expect(decorated.extra_fields).to include(:occams_id)
    end
  end

  context "#ordered_fields" do
    it "should be ordered by oe_number" do
      expect(decorated.ordered_fields).to include(:oe_number)
    end
  end
end
