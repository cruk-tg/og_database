require 'spec_helper'

describe PathologyDecorator.new(Pathology.new) do
  it_behaves_like "a reportable object", :with => 23.cells
end
