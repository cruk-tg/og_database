require 'spec_helper'

describe FollowUpDecorator.new(FollowUp.new) do
  it_behaves_like "a reportable object", :with => 10.cells
end
