require 'spec_helper'

describe ExposureDecorator.new(Exposure.new) do
  it_behaves_like "a reportable object", :with => 31.cells
end
