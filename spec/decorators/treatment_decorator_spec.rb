require 'spec_helper'

describe TreatmentDecorator.new(Treatment.new) do
  it_behaves_like "a reportable object", :with => 19.cells
end
