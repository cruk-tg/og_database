class Fixnum
  def cells
    self
  end
  def cell
    self
  end
end

shared_examples "a reportable object" do |params|
  it "provides report data" do
    expect(subject).to respond_to(:xls_data)
  end

  if params && params.include?(:with)
    it "returns #{params[:with]} cells" do
      expect(subject.xls_data.count).to eq(params[:with])
    end
  end
end
