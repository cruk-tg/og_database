shared_examples "a demographic related controller" do
  before(:all) do
    @model = Object.class_eval(described_class.name.gsub("Controller","").singularize)
    @model_sym = @model.name.underscore.to_sym
  end

  before(:each) do
    @demographic = FactoryBot.create(:demographic)
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new', params: {:demographic_id => @demographic.id}
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "returns http success" do
      params = { :id => FactoryBot.create(@model_sym).id, :demographic_id => @demographic.id }
      get 'edit', params: params
      response.should be_success
    end
  end

  describe "POST create" do
    context "valid exposure" do
      it "create a new exposure" do
        expect {
          attrs = FactoryBot.build(@model_sym).attributes.delete_if { |k,v| ["id","created_at", "updated_at", "demographic_id"].include?(k) }
          post_vars = {@model_sym => attrs, :demographic_id => @demographic.id }
          post :create, params: post_vars
        }.to change(@model,:count).by(1)
      end
    end
  end
  describe "PUT update'" do
    before(:each) do
      @model_instance = FactoryBot.create(@model_sym)
    end

    context "valid exposure" do
      it "update a exposure" do
        model_instance_attrs = @model_instance.attributes.delete_if { |k,v| ["id", "created_at", "updated_at", "demographic_id"].include?(k) }
        puts_vars = {:id => @model_instance.id, @model_sym => model_instance_attrs, :demogaphic_id => @demographic.id}
        put :update, params: puts_vars
        assigns(:form_model).should eq(@model_instance)
      end
    end
  end
end
