require 'nokogiri'

RSpec::Matchers.define :have_xml do |xpath, text|
  match do |body|
    doc = Nokogiri::XML::Document.parse body
    nodes = doc.xpath(xpath)
    nodes.should_not be_empty
    if text
      nodes.each do |node|
        node.content.should == text
      end
    end
    true
  end

  failure_message_for_should do |body|
    "expected to find xml tag #{xpath} with #{text}"
  end

  failure_message_for_should_not do |response|
    "expected not to find xml tag #{xpath} with #{text}"
  end

  description do
    "have xml tag #{xpath}"
  end
end
