require 'nokogiri'

module XMLSpreadsheet
  def column_with_name(xml,name)
    doc = Nokogiri::XML::Document.parse xml
    nodes = doc.xpath("/ss:Workbook/ss:Worksheet[@ss:Name='Data']/ss:Table/ss:Row[1]/ss:Cell/ss:Data[text()='#{name}']")
    nodes && (nodes[0].parent.path =~ /.*\[(\d+)\]$/)
  $1
  end

  def self.test_values
    { 'exposure' => { 
        field: 'pathology_number',
        table: 'Pathology',
        value: 'UB000000' },
      'staging' => { 
        field: 'pathology_number',
        table: 'Pathology',
        value: 'UB000000'
      },
      'treatment' => {
        field: 'tma',
        table: 'Ffpe',
        value: 'xxx'
      },
      'pathology' => {
        field: 'tma',
        table: 'Ffpe',
        value: 'xxx'
      },
      'follow_up' => {
        field: 'tma',
        table: 'Ffpe',
        value: 'xxx'
      }
    }
  end

end

