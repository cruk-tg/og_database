# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'database_cleaner'
require 'capybara/rspec'
require 'rack/utils'
require 'capybara/poltergeist'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

Capybara.register_driver :phatomjs do |app|
  Capybara::Poltergeist::Driver.new(app, :browser => :chrome)
end

RSpec.configure do |config|
  config.example_status_persistence_file_path = "examples.txt"
  config.expect_with(:rspec) { |c| c.syntax = [:should, :expect] }
  config.mock_with :mocha
  config.infer_base_class_for_anonymous_controllers = false

  config.before(:suite) do
    DatabaseCleaner.strategy = :deletion
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.before(:each, type: :controller) do
    user = User.create! :username => "username"
    session['cas'] = { 'user' => user.username, 'extra_attributes' => {} }
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end
