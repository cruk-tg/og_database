require 'spec_helper'

describe Sample do
  let(:demographic) { FactoryBot.build(:demographic)}
  let(:sample) { FactoryBot.build(:sample)}
  let(:participant_label) { "OE000007" }

  it "allows unknown fields to be added to extra" do
    extra = sample.extra
    extra.update(:field => 'value')
    sample.extra = extra
    sample.extra.should have_key(:field)
  end

  it "extra attributes are presisted" do
    extra = sample.extra
    extra.update(:field => 'value')
    sample.extra = extra
    sample.save
    Sample.find_by_label(sample.label).extra.should have_key(:field)
  end

  it "first_or_initalize (init)" do
    s = Sample.where(:label => participant_label.next).first_or_initialize
    extra = sample.extra
    extra.update(:field => 'value')
    sample.extra = extra
    s.save.should be true
  end

  it "first_or_initialize (first)" do
    sample.save
    s = Sample.where(:label => sample.label).first_or_initialize
    extra = sample.extra
    extra.update(:field => 'value')
    sample.extra = extra
    s.save.should be true
  end

  it "return nil for a container that has no storage location" do
    sample_without_location = FactoryBot.create(:sample, :demographic_id => demographic.id, :label => participant_label.concat('A'), :extra => nil)
    sample_without_location.storage_location.should be_nil
  end

  it "has a storage location when the sample has a storage location" do
    storage_location = 'A Box'
    sample_with_location = FactoryBot.create(:sample, :demographic_id => demographic.id, :label => participant_label.concat('A'), :extra => {:storage_location => storage_location})
    sample_with_location.storage_location.should eq(storage_location)
  end

  it "for storage locations that contain the string 'In Transit'  that string is removed" do
    storage_location = 'In Transit Container TR001'
    sample_with_location = FactoryBot.create(:sample, :demographic_id => demographic.id, :label => participant_label.concat('A'), :extra => {:storage_location => storage_location})
    sample_with_location.storage_location.should eq('TR001')
  end

  it "has an interval in minutes between collected and frozen" do
    a_sample = FactoryBot.build :sample
    a_sample.frozen_time = DateTime.parse('2013-01-01 10:15')
    a_sample.collection_time = DateTime.parse('2013-01-01 10:00')
    a_sample.interval.should eq(15)
  end
end
