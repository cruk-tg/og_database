require 'spec_helper'

describe FollowUp do
  let(:follow_up) { FactoryBot.create :follow_up }

  #TODO test for presence of validator move test to lib
  it "DOD should be less than DLS" do
    test = FactoryBot.build :follow_up
    test.dod = Date.today
    test.dls = Date.today.tomorrow
    test.should_not be_valid
  end

  it "should not allow a DOD and a censor_value not alive" do
    test = FactoryBot.build :follow_up
    test.dod = Date.today
    test.censor_value = "Alive"
    test.should_not be_valid
  end
end
