require 'spec_helper'

describe Exposure do
  before do
    @exposure = FactoryBot.create(:exposure)
  end

  it "should calculate BMI by Weight/Height^2" do
    @exposure.weight = 95
    @exposure.height = 1.99
    @exposure.bmi.should eq((100*95/1.99**2).floor.to_f / 100)
  end
end
