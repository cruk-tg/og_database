require 'spec_helper'

describe Demographic do
  before(:each) do
    FactoryBot.create(:demographic)
  end

  let(:demographic) { FactoryBot.build(:demographic) }

  it "should reject invalid OCCAMS IDs" do
    demographic.occams_id = "ABC/AC/01"
    demographic.should_not be_valid
  end

  describe "ECMC ID" do
    it "can start with 'OC'" do
      demographic.oe_number = "OC000001"
      demographic.should be_valid
    end

    it "can start with 'OE'" do
      demographic.oe_number = "OE000001"
      demographic.should be_valid
    end

    it "cannot start with anything else" do
      demographic.oe_number = "ab000001"
      demographic.should_not be_valid
    end
  end

  it "should reject invalid CHIs" do
    demographic.chi = "aaa"
    demographic.should_not be_valid
  end
end
