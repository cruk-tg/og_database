require 'rubygems'
require 'nokogiri'

def column_with_name(xml,name)
  doc = Nokogiri::XML::Document.parse xml
  nodes = doc.xpath("/ss:Workbook/ss:Worksheet[@ss:Name='Data']/ss:Table/ss:Row[1]/ss:Cell/ss:Data[text()='#{name}']")
  nodes && (nodes[0].parent.path =~ /.*\[(\d+)\]$/)
  $1
end


file = File.open('/home/pmitche2/Downloads/reports.xml')
puts column_with_name file, 'date_of_ecmc_consent'


