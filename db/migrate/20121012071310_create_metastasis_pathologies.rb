class CreateMetastasisPathologies < ActiveRecord::Migration
  def change
    create_table :metastasis_pathologies do |t|
      t.string :name

      t.timestamps
    end
  end
end
