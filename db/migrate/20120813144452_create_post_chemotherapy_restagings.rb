class CreatePostChemotherapyRestagings < ActiveRecord::Migration
  def change
    create_table :post_chemotherapy_restagings do |t|
      t.integer :pcr_type_id
      t.integer :findings_id
      t.integer :demographic_id
      t.integer :staging_id

      t.timestamps
    end
  end
end
