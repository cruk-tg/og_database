class CreateTnmMs < ActiveRecord::Migration
  def change
    create_table :tnm_ms do |t|
      t.string :name

      t.timestamps
    end
  end
end
