class AddSignetRingCellsToPathology < ActiveRecord::Migration
  def change
    add_column :pathologies, :signet_ring_cells, :string
  end
end
