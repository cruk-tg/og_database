class CreateRadiotherapyIntentLookups < ActiveRecord::Migration
  def change
    create_table :radiotherapy_intent_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
