class AddTumourSizesToStagingEndoscopicUltrasounds < ActiveRecord::Migration
  def change
    add_column :staging_endoscopic_ultrasounds, :tumour_from, :float
    add_column :staging_endoscopic_ultrasounds, :tumour_to, :float
  end
end
