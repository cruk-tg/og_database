class CreateExerciseTolerances < ActiveRecord::Migration
  def change
    create_table :exercise_tolerances do |t|
      t.string :name

      t.timestamps
    end
  end
end
