class CreateLocalRecurrences < ActiveRecord::Migration
  def change
    create_table :local_recurrences do |t|
      t.string :name

      t.timestamps
    end
  end
end
