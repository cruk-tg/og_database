class AddPostopChemoCommencedDateToTreatments < ActiveRecord::Migration
  def change
    add_column :treatments, :postop_chemo_commenced_date, :date
  end
end
