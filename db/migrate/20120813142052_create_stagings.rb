class CreateStagings < ActiveRecord::Migration
  def change
    create_table :stagings do |t|
      t.date :date_of_diagnosis
      t.integer :tumour_location_id
      t.integer :histology_id
      t.integer :demographic_id
      t.integer :laparoscopy_id

      t.timestamps
    end
  end
end
