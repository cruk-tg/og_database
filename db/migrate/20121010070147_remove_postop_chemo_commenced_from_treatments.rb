class RemovePostopChemoCommencedFromTreatments < ActiveRecord::Migration
  def up
    remove_column :treatments, :postop_chemo_commenced
  end

  def down
    add_column :treatments, :postop_chemo_commenced, :integer
  end
end
