class CreateSmokers < ActiveRecord::Migration
  def change
    create_table :smokers do |t|
      t.string :name

      t.timestamps
    end
  end
end
