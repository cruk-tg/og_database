class CreateSurgeryConsultants < ActiveRecord::Migration
  def change
    create_table :surgery_consultants do |t|
      t.string :name

      t.timestamps
    end
  end
end
