class CreateSamples < ActiveRecord::Migration
  def change
    create_table :samples do |t|
      t.string :label
      t.string :barcode
      t.integer :demographic_id
      t.text :extra

      t.timestamps
    end
  end
end
