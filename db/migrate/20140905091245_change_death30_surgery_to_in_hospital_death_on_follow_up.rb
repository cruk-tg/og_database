class ChangeDeath30SurgeryToInHospitalDeathOnFollowUp < ActiveRecord::Migration
  def up
    rename_column :follow_ups, :death_30_surgery, :in_hospital_death
  end

  def down
  end
end
