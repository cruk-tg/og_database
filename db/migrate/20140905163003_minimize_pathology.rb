class MinimizePathology < ActiveRecord::Migration
  include Migration::Flatten

  BELONGS_TO = {:tumour_location => TumourLocation, :histology => Histology, :grade => Grading, :resection => ResectionLookup, :lymphovascalar_invasion => YesNoUnknown, :perineural_invasion => YesNoUnknown, :venous_invasion => YesNoUnknown, :tnm_t => TnmT, :tnm_n => TnmN, :tnm_m => TnmM, :nodes_on_both_sides => YesNoUnknown, :histological_response => HistologicalResponse}
  TABLENAME = :pathologies

  def change
    flatten TABLENAME, BELONGS_TO
  end
end
