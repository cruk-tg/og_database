class AddRecurrenceToFollowUp < ActiveRecord::Migration
  def change
    add_column :follow_ups, :recurrence, :string
  end
end
