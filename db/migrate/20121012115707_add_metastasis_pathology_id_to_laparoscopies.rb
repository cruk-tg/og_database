class AddMetastasisPathologyIdToLaparoscopies < ActiveRecord::Migration
  def change
    add_column :laparoscopies, :metastasis_pathology_id, :integer
  end
end
