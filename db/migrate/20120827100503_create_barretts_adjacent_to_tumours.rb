class CreateBarrettsAdjacentToTumours < ActiveRecord::Migration
  def change
    create_table :barretts_adjacent_to_tumours do |t|
      t.integer :pathology_id
      t.integer :barretts_adjacent_lookup_id

      t.timestamps
    end
  end
end
