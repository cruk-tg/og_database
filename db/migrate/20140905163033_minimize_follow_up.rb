class MinimizeFollowUp < ActiveRecord::Migration
  include Migration::Flatten

  BELONGS_TO = {:censor_value => CensorValue, :local_recurrence => LocalRecurrence}
  TABLENAME  = :follow_ups

  def change
    flatten TABLENAME, BELONGS_TO
  end
end
