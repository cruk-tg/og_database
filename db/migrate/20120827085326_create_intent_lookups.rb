class CreateIntentLookups < ActiveRecord::Migration
  def change
    create_table :intent_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
