class ChangePathologyNumberToResectionPathologyNumberOnPathology < ActiveRecord::Migration
  def up
    rename_column :pathologies, :pathology_number, :resection_pathology_number
  end

  def down
  end
end
