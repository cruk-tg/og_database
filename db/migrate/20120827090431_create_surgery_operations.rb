class CreateSurgeryOperations < ActiveRecord::Migration
  def change
    create_table :surgery_operations do |t|
      t.string :name
      t.string :phase

      t.timestamps
    end
  end
end
