class CreateChemotherapyWhyOffLookups < ActiveRecord::Migration
  def change
    create_table :chemotherapy_why_off_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
