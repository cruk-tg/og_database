class CreateLengthOfUses < ActiveRecord::Migration
  def change
    create_table :length_of_uses do |t|
      t.string :name

      t.timestamps
    end
  end
end
