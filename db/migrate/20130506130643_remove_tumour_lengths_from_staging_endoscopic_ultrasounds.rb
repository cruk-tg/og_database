class RemoveTumourLengthsFromStagingEndoscopicUltrasounds < ActiveRecord::Migration
  def up
    remove_column :staging_endoscopic_ultrasounds, :tumour_length_x
    remove_column :staging_endoscopic_ultrasounds, :tumour_length_y
    remove_column :staging_endoscopic_ultrasounds, :tumour_length_z
  end

  def down
    add_column :staging_endoscopic_ultrasounds, :tumour_length_z, :integer
    add_column :staging_endoscopic_ultrasounds, :tumour_length_y, :integer
    add_column :staging_endoscopic_ultrasounds, :tumour_length_x, :integer
  end
end
