class AddIntersectionColumnsToPresentingComplaint < ActiveRecord::Migration
  def change
    add_column :presenting_complaints, :exposure_id, :integer
    add_column :presenting_complaints, :presenting_complaint_lookup_id, :integer
  end
end
