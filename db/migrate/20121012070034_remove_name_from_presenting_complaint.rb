class RemoveNameFromPresentingComplaint < ActiveRecord::Migration
  def up
    remove_column :presenting_complaints, :name
  end

  def down
    add_column :presenting_complaints, :name, :string
  end
end
