class RemoveMetsLocationIdFromFinalMdtStages < ActiveRecord::Migration
  def up
    remove_column :final_mdt_stages, :mets_location_id
  end

  def down
    add_column :final_mdt_stages, :mets_location_id, :integer
  end
end
