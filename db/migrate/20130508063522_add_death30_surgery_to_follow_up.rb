class AddDeath30SurgeryToFollowUp < ActiveRecord::Migration
  def change
    add_column :follow_ups, :death_30_surgery, :boolean
  end
end
