class CreateEvidenceForRecurrenceLookups < ActiveRecord::Migration
  def change
    create_table :evidence_for_recurrence_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
