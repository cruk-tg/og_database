class CreateExposures < ActiveRecord::Migration
  def change
    create_table :exposures do |t|
      t.integer :demographic_id
      t.integer :known_barretts_id
      t.integer :barretts_length
      t.integer :prague_classification_c
      t.integer :prague_classification_m
      t.integer :smoker_id
      t.string :smoker_packs
      t.string :smoker_years
      t.integer :alcohol
      t.float :egfr
      t.float :pulmonary_function_fev1_lm
      t.float :pulmonary_function_fev1_p
      t.float :pulmonary_function_fvc_lm
      t.float :pulmonary_function_fvc_p
      t.float :pulmonary_function_fev1_fvc_p
      t.float :albumin
      t.float :haemoglobin
      t.float :weight_loss
      t.integer :weight_loss_weeks
      t.float :height
      t.float :weight
      t.float :bmi
      t.integer :exercise_tolerance_id
      t.integer :asa_grade_id
      t.integer :presenting_complaint_id
      t.integer :referral_source_id

      t.timestamps
    end
  end
end
