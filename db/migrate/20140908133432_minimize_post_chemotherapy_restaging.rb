class MinimizePostChemotherapyRestaging < ActiveRecord::Migration
  include Migration::Flatten

  BELONGS_TO = {:pcr_type => PostChemotherapyRestagingLookup, :finding => Response}
  TABLENAME  = :post_chemotherapy_restagings

  def change
    flatten TABLENAME, BELONGS_TO
  end
end
