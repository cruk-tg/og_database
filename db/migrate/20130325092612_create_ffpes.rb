class CreateFfpes < ActiveRecord::Migration
  def change
    create_table :ffpes do |t|
      t.integer :demographic_id
      t.string :tma
      t.string :tma_cores
      t.string :core_location
      t.string :path_response_slides
      t.string :slide_location

      t.timestamps
    end
  end
end
