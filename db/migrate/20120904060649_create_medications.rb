class CreateMedications < ActiveRecord::Migration
  def change
    create_table :medications do |t|
      t.integer :exposure_id
      t.integer :drug_id
      t.integer :length_of_use_id

      t.timestamps
    end
  end
end
