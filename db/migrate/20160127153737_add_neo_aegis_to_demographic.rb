class AddNeoAegisToDemographic < ActiveRecord::Migration
  def change
    add_column :demographics, :neo_aegis, :string
  end
end
