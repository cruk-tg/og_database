class CreateTnmTs < ActiveRecord::Migration
  def change
    create_table :tnm_ts do |t|
      t.string :name

      t.timestamps
    end
  end
end
