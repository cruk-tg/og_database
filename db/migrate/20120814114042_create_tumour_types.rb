class CreateTumourTypes < ActiveRecord::Migration
  def change
    create_table :tumour_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
