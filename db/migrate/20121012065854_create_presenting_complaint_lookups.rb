class CreatePresentingComplaintLookups < ActiveRecord::Migration
  def change
    create_table :presenting_complaint_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
