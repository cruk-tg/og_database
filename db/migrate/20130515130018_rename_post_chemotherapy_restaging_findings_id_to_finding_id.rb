class RenamePostChemotherapyRestagingFindingsIdToFindingId < ActiveRecord::Migration
  def up
    rename_column :post_chemotherapy_restagings, :findings_id, :finding_id
  end

  def down
    rename_column :post_chemotherapy_restagings, :finding_id, :findings_id
  end
end
