class RemoveNameFromEvidenceForRecurrences < ActiveRecord::Migration
  def up
    remove_column :evidence_for_recurrences, :name
  end

  def down
    add_column :evidence_for_recurrences, :name, :string
  end
end
