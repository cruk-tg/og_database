class CreateLaparoscopies < ActiveRecord::Migration
  def change
    create_table :laparoscopies do |t|
      t.text :macroscopic_findings
      t.date :date
      t.integer :tumour_location_id
      t.integer :dmpd_id
      t.integer :ascites_id
      t.integer :mom_id
      t.integer :primary_tumour_extension_id
      t.integer :washings_report_id
      t.integer :staging_id

      t.timestamps
    end
  end
end
