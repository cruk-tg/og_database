class MinimizeStaging < ActiveRecord::Migration
  include Migration::Flatten

  BELONGS_TO = {:tumour_location => TumourLocation, :histology => Histology}
  TABLENAME = :stagings

  def change
    flatten TABLENAME, BELONGS_TO
  end
end
