class MinimizeStagingCt < ActiveRecord::Migration
  include Migration::Flatten

  BELONGS_TO = {:tnm_t => TnmT, :tnm_n => TnmN, :tnm_m => TnmM}
  TABLENAME  = :staging_cts

  def change
    flatten TABLENAME, BELONGS_TO
  end
end
