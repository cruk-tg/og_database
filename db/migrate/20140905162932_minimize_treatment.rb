class MinimizeTreatment < ActiveRecord::Migration
  include Migration::Flatten

  BELONGS_TO = {:preop_chemo_reg => ChemotherapyRegimen, :postop_chemo_reg => ChemotherapyRegimen, :treatment_intent => TreatmentIntentLookup, :preop_chemo_type => IntentLookup, :radio_type => RadiotherapyIntentLookup, :patient_received_deuterium => YesNoUnknown, :surgery_happened => YesNoUnknown, :surgery_consultant => SurgeryConsultant, :surgery_operation => SurgeryOperation, :abdominal_phase => SurgeryOperation, :thoracic_phase => SurgeryOperation, :chemotherapy_why_off_lookup => ChemotherapyWhyOffLookup}
  TABLENAME = :treatments

  def change
    rename_column :treatments, "preop_chemo_whyoff_id", "chemotherapy_why_off_lookup_id"
    rename_column :treatments, :intent_id, :treatment_intent_id

    flatten TABLENAME, BELONGS_TO
  end
end
