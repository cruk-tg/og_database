class CreateFieldDefinitions < ActiveRecord::Migration
  def change
    create_table :field_definitions do |t|
      t.string :table_name
      t.string :field_name
      t.string :definition

      t.timestamps null: false
    end
  end
end
