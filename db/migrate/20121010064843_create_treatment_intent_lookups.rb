class CreateTreatmentIntentLookups < ActiveRecord::Migration
  def change
    create_table :treatment_intent_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
