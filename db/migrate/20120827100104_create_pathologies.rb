class CreatePathologies < ActiveRecord::Migration
  def change
    create_table :pathologies do |t|
      t.string :pathology_number
      t.integer :tumour_location_id
      t.integer :histology_id
      t.integer :grade_id
      t.float :maximum_tumour_diameter
      t.float :distance_to_proximal_margin
      t.float :distance_to_distal_margin
      t.float :distance_to_circumferential_resection_margin
      t.string :resection_id
      t.integer :lymphovascalar_invasion_id
      t.integer :perineural_invasion_id
      t.integer :venous_invasion_id
      t.integer :tnm_t_id
      t.integer :tnm_n_id
      t.integer :tnm_m_id
      t.integer :number_of_nodes
      t.integer :postive_nodes
      t.string :location_of_involved_nodes
      t.integer :nodes_on_both_sides_id
      t.integer :demographic_id
      t.integer :histological_response_id

      t.timestamps
    end
  end
end
