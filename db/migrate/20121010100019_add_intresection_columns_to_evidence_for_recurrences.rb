class AddIntresectionColumnsToEvidenceForRecurrences < ActiveRecord::Migration
  def change
    add_column :evidence_for_recurrences, :follow_up_id, :integer
    add_column :evidence_for_recurrences, :evidence_for_recurrence_lookup_id, :integer
  end
end
