class CreateEvidenceForRecurrences < ActiveRecord::Migration
  def change
    create_table :evidence_for_recurrences do |t|
      t.string :name

      t.timestamps
    end
  end
end
