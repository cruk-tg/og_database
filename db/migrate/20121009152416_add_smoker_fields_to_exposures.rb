class AddSmokerFieldsToExposures < ActiveRecord::Migration
  def change
    add_column :exposures, :smoker_years, :float
    add_column :exposures, :smoker_packs, :float
    add_column :exposures, :smoker_pack_years, :float
  end
end
