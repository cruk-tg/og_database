class AddRquestedFieldsToSamples < ActiveRecord::Migration
  def change
    change_table :samples do |t|
      t.string :sample_class
      t.string :sample_type
      t.datetime :collection_time
      t.datetime :frozen_time
      t.string :haemolysed
      t.string :event_position
      t.string :location
      t.string :section_undertaken
      t.string :histology
      t.string :path_response_slides
      t.string :histology_text
      t.string :on_tma
      t.string :tma_cores
      t.string :core_location
    end
  end
end
