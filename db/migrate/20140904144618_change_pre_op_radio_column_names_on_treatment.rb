class ChangePreOpRadioColumnNamesOnTreatment < ActiveRecord::Migration
  def up
    rename_column :treatments, :preop_radio_complications, :radio_complications
    rename_column :treatments, :preop_radio_dose,          :radio_dose
    rename_column :treatments, :preop_radio_fractions,     :radio_fractions
    rename_column :treatments, :preop_radio_type_id,          :radio_type_id
  end

  def down
  end
end
