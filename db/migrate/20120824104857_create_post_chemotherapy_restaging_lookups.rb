class CreatePostChemotherapyRestagingLookups < ActiveRecord::Migration
  def change
    create_table :post_chemotherapy_restaging_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
