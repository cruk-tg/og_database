class CreateYesNoUnknowns < ActiveRecord::Migration
  def change
    create_table :yes_no_unknowns do |t|
      t.string :name

      t.timestamps
    end
  end
end
