class CreatePrimaryTumourExtensions < ActiveRecord::Migration
  def change
    create_table :primary_tumour_extensions do |t|
      t.string :name

      t.timestamps
    end
  end
end
