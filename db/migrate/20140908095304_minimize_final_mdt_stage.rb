class MinimizeFinalMdtStage < ActiveRecord::Migration
  include Migration::Flatten

  BELONGS_TO = {:tnm_t => TnmT, :tnm_n => TnmN, :tnm_m => TnmM}
  TABLENAME  = :final_mdt_stages

  def change
    flatten TABLENAME, BELONGS_TO
  end
end
