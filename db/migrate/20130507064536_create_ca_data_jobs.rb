class CreateCaDataJobs < ActiveRecord::Migration
  def change
    create_table :ca_data_jobs do |t|
      t.datetime :started
      t.string :status

      t.timestamps
    end
  end
end
