class CreateFamilyHistoryOfOtherCancers < ActiveRecord::Migration
  def change
    create_table :family_history_of_other_cancers do |t|
      t.integer :yes_no_unk_id
      t.integer :relationship_id
      t.integer :exposure_id

      t.timestamps
    end
  end
end
