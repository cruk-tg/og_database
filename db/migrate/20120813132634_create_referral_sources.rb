class CreateReferralSources < ActiveRecord::Migration
  def change
    create_table :referral_sources do |t|
      t.string :name

      t.timestamps
    end
  end
end
