class CreateCensorValues < ActiveRecord::Migration
  def change
    create_table :censor_values do |t|
      t.string :name

      t.timestamps
    end
  end
end
