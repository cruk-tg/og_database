class RemovePresentingComplaintIdFromExposures < ActiveRecord::Migration
  def up
    remove_column :exposures, :presenting_complaint_id
  end

  def down
    add_column :exposures, :presenting_complaint_id, :integer
  end
end
