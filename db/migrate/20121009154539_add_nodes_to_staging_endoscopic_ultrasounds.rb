class AddNodesToStagingEndoscopicUltrasounds < ActiveRecord::Migration
  def change
    add_column :staging_endoscopic_ultrasounds, :nodes, :string
  end
end
