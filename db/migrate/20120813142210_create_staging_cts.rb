class CreateStagingCts < ActiveRecord::Migration
  def change
    create_table :staging_cts do |t|
      t.text :report
      t.integer :tnm_t_id
      t.integer :tnm_n_id
      t.integer :nodes
      t.integer :positive_nodes
      t.integer :tnm_m_id
      t.integer :mets_location_id
      t.integer :staging_id
      
      t.timestamps
    end
  end
end
