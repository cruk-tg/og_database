class CreateResectionLookups < ActiveRecord::Migration
  def change
    create_table :resection_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
