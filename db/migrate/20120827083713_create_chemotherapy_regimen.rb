class CreateChemotherapyRegimen < ActiveRecord::Migration
  def change
    create_table :chemotherapy_regimen do |t|
      t.string :name
      t.string :agents_csv

      t.timestamps
    end
  end
end
