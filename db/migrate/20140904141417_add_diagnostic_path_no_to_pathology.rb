class AddDiagnosticPathNoToPathology < ActiveRecord::Migration
  def change
    add_column :pathologies, :diagnostic_path_no, :string
  end
end
