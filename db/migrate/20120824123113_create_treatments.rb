class CreateTreatments < ActiveRecord::Migration
  def change
    create_table :treatments do |t|
      t.integer :demographic_id
      t.integer :intent_id
      t.integer :preop_chemo_type_id
      t.string :preop_chemo_reg_id
      t.integer :preop_chemo_whyoff_id
      t.integer :preop_radio_type_id
      t.integer :preop_radio_dose
      t.integer :preop_radio_fractions
      t.string :preop_radio_complications
      t.integer :patient_received_deuterium_id
      t.integer :surgery_happened_id
      t.date :surgery_date
      t.integer :surgery_consultant_id
      t.integer :surgery_operation_id
      t.integer :abdominal_phase_id
      t.integer :thoracic_phase_id
      t.integer :postop_chemo_reg_id
      t.integer :postop_chemo_commenced
      t.string :preop_chemo_complications

      t.timestamps
    end
  end
end
