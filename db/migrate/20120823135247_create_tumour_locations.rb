class CreateTumourLocations < ActiveRecord::Migration
  def change
    create_table :tumour_locations do |t|
      t.string :name

      t.timestamps
    end
  end
end
