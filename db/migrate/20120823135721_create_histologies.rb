class CreateHistologies < ActiveRecord::Migration
  def change
    create_table :histologies do |t|
      t.string :name

      t.timestamps
    end
  end
end
