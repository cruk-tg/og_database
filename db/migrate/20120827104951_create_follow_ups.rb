class CreateFollowUps < ActiveRecord::Migration
  def change
    create_table :follow_ups do |t|
      t.integer :demographic_id
      t.integer :censor_value_id
      t.date :dod
      t.date :dls
      t.date :date_of_recurrence
      t.string :region_of_recurrence
      t.integer :local_recurrence_id
      t.integer :evidence_for_recurrence_id

      t.timestamps
    end
  end
end
