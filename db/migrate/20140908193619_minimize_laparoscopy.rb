class MinimizeLaparoscopy < ActiveRecord::Migration
  include Migration::Flatten

  BELONGS_TO = {:tumour_location => TumourLocation, :dmpd => YesNoUnknown, :ascites => YesNoUnknown, :mom => YesNoUnknown, :primary_tumour_extension => PrimaryTumourExtension, :washings_report => WashingsReport, :metastasis_pathology => MetastasisPathology}
  TABLENAME = :laparoscopies

  def change
    flatten(TABLENAME, BELONGS_TO)
  end
end
