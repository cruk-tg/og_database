#FIXME class name should CreateWashingsReports
class CreateWashingsReports < ActiveRecord::Migration
  def change
    create_table :washings_reports do |t|
      t.string :name

      t.timestamps
    end
  end
end
