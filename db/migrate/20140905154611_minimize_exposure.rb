class MinimizeExposure < ActiveRecord::Migration
  include Migration::Flatten

  BELONGS_TO = {
    :smoker => Smoker,
    :known_barretts => YesNoUnknown,
    :previous_alcohol_excess => YesNoUnknown,
    :exercise_tolerance => ExerciseTolerance,
    :asa_grade => AsaGrade,
    :referral_source => ReferralSource
  }
  TABLENAME = :exposures

  def change
    flatten(TABLENAME, BELONGS_TO)
  end
end
