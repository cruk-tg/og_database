class CreateFinalMdtStages < ActiveRecord::Migration
  def change
    create_table :final_mdt_stages do |t|
      t.integer :demographic_id
      t.integer :tnm_t_id
      t.integer :tnm_n_id
      t.integer :tnm_m_id
      t.integer :mets_location_id
      t.integer :staging_id

      t.timestamps
    end
  end
end
