class AlterWeightLossWeeksToString < ActiveRecord::Migration[5.1]
  def change
    change_column :exposures, :weight_loss_weeks, :string
  end
end
