class RemoveLaparoscopyIdFromStagings < ActiveRecord::Migration
  def up
    remove_column :stagings, :laparoscopy_id
  end

  def down
    add_column :stagings, :laparoscopy_id, :integer
  end
end
