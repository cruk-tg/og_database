class RemoveSmokerYearsFromExposures < ActiveRecord::Migration
  def up
    remove_column :exposures, :smoker_years
  end

  def down
    add_column :exposures, :smoker_years, :string
  end
end
