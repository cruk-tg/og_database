class CreatePreviousBarrettManagements < ActiveRecord::Migration
  def change
    create_table :previous_barrett_managements do |t|
      t.string :name
      t.integer :exposure_id
      t.integer :barrett_managment_lookup_id

      t.timestamps
    end
  end
end
