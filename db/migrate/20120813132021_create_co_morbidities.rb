class CreateCoMorbidities < ActiveRecord::Migration
  def change
    create_table :co_morbidities do |t|
      t.integer :co_morbidity_lookup_id
      t.integer :exposure_id

      t.timestamps
    end
  end
end
