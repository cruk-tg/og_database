class AddPostOpIntentToTreatment < ActiveRecord::Migration
  def change
    add_column :treatments, :post_op_intent, :string
  end
end
