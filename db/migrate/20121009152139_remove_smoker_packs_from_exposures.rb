class RemoveSmokerPacksFromExposures < ActiveRecord::Migration
  def up
    remove_column :exposures, :smoker_packs
  end

  def down
    add_column :exposures, :smoker_packs, :string
  end
end
