class CreateHistologicalResponses < ActiveRecord::Migration
  def change
    create_table :histological_responses do |t|
      t.string :name

      t.timestamps
    end
  end
end
