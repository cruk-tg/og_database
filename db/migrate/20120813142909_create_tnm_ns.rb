class CreateTnmNs < ActiveRecord::Migration
  def change
    create_table :tnm_ns do |t|
      t.string :name

      t.timestamps
    end
  end
end
