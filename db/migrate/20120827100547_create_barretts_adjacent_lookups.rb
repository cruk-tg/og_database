class CreateBarrettsAdjacentLookups < ActiveRecord::Migration
  def change
    create_table :barretts_adjacent_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
