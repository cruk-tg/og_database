class AddMetsLocationToFinalMdtStages < ActiveRecord::Migration
  def change
    add_column :final_mdt_stages, :mets_location, :string
  end
end
