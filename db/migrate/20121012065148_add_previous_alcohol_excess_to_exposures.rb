class AddPreviousAlcoholExcessToExposures < ActiveRecord::Migration
  def change
    add_column :exposures, :previous_alcohol_excess_id, :integer
  end
end
