class CreateDemographics < ActiveRecord::Migration
  def change
    create_table :demographics do |t|
      t.string :hospital_number
      t.string :chi
      t.date :dob
      t.integer :gender_id
      t.string :gp_name
      t.string :gp_address1
      t.string :gp_address2
      t.string :gp_address3
      t.string :gp_address4
      t.string :gp_phone_number
      t.date :date_of_ecmc_consent
      t.date :date_of_occams_consent
      t.string :occams_id
      t.string :oe_number

      t.timestamps
    end
  end
end
