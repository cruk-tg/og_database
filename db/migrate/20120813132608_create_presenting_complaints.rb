class CreatePresentingComplaints < ActiveRecord::Migration
  def change
    create_table :presenting_complaints do |t|
      t.string :name

      t.timestamps
    end
  end
end
