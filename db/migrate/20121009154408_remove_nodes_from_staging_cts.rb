class RemoveNodesFromStagingCts < ActiveRecord::Migration
  def up
    remove_column :staging_cts, :nodes
  end

  def down
    add_column :staging_cts, :nodes, :integer
  end
end
