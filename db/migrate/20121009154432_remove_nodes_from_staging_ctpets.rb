class RemoveNodesFromStagingCtpets < ActiveRecord::Migration
  def up
    remove_column :staging_ctpets, :nodes
  end

  def down
    add_column :staging_ctpets, :nodes, :integer
  end
end
