class CreateOtherInvestigations < ActiveRecord::Migration
  def change
    create_table :other_investigations do |t|
      t.integer :staging_id
      t.text :report
      t.integer :metastatsis_id

      t.timestamps
    end
  end
end
