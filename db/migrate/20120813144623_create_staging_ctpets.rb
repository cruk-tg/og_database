class CreateStagingCtpets < ActiveRecord::Migration
  def change
    create_table :staging_ctpets do |t|
      t.text :report
      t.integer :tnm_t_id
      t.integer :tnm_n_id
      t.integer :tnm_m_id
      t.integer :positive_node_location_id
      t.integer :nodes
      t.integer :demographic_id
      t.integer :staging_id

      t.timestamps
    end
  end
end
