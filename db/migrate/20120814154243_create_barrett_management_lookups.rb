class CreateBarrettManagementLookups < ActiveRecord::Migration
  def change
    create_table :barrett_management_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
