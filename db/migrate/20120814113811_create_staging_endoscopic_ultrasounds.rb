class CreateStagingEndoscopicUltrasounds < ActiveRecord::Migration
  def change
    create_table :staging_endoscopic_ultrasounds do |t|
      t.integer :demographic_id
      t.text :report
      t.integer :tumour_length_x
      t.integer :tumour_length_y
      t.integer :tumour_length_z
      t.integer :tumour_type_id
      t.integer :tnm_t_id
      t.integer :tnm_n_id
      t.integer :tnm_m_id
      t.integer :mets_location_id
      t.integer :fna_location_id
      t.integer :staging_id

      t.timestamps
    end
  end
end
