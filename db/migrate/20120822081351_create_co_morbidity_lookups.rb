class CreateCoMorbidityLookups < ActiveRecord::Migration
  def change
    create_table :co_morbidity_lookups do |t|
      t.string :name

      t.timestamps
    end
  end
end
