# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180726084949) do

  create_table "asa_grades", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "audits", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.datetime "created_at"
    t.string "request_uuid"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "barrett_management_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "barretts_adjacent_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "barretts_adjacent_to_tumours", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "pathology_id"
    t.integer "barretts_adjacent_lookup_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ca_data_jobs", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "started"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "censor_values", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chemotherapy_regimen", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "agents_csv"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chemotherapy_why_off_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "co_morbidities", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "co_morbidity_lookup_id"
    t.integer "exposure_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "co_morbidity_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "demographics", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "hospital_number"
    t.string "chi"
    t.date "dob"
    t.integer "gender_id"
    t.string "gp_name"
    t.string "gp_address1"
    t.string "gp_address2"
    t.string "gp_address3"
    t.string "gp_address4"
    t.string "gp_phone_number"
    t.date "date_of_ecmc_consent"
    t.date "date_of_occams_consent"
    t.string "occams_id"
    t.string "oe_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "neo_aegis"
  end

  create_table "drugs", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "evidence_for_recurrence_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "evidence_for_recurrences", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "follow_up_id"
    t.integer "evidence_for_recurrence_lookup_id"
  end

  create_table "exercise_tolerances", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exposures", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "demographic_id"
    t.integer "barretts_length"
    t.integer "prague_classification_c"
    t.integer "prague_classification_m"
    t.integer "alcohol"
    t.float "egfr", limit: 24
    t.float "pulmonary_function_fev1_lm", limit: 24
    t.float "pulmonary_function_fev1_p", limit: 24
    t.float "pulmonary_function_fvc_lm", limit: 24
    t.float "pulmonary_function_fvc_p", limit: 24
    t.float "pulmonary_function_fev1_fvc_p", limit: 24
    t.float "albumin", limit: 24
    t.float "haemoglobin", limit: 24
    t.float "weight_loss", limit: 24
    t.string "weight_loss_weeks"
    t.float "height", limit: 24
    t.float "weight", limit: 24
    t.float "bmi", limit: 24
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "smoker_years", limit: 24
    t.float "smoker_packs", limit: 24
    t.float "smoker_pack_years", limit: 24
    t.string "smoker"
    t.string "known_barretts"
    t.string "previous_alcohol_excess"
    t.string "exercise_tolerance"
    t.string "asa_grade"
    t.string "referral_source"
  end

  create_table "family_history_of_og_cancers", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "yes_no_unk_id"
    t.integer "relationship_id"
    t.integer "exposure_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "family_history_of_other_cancers", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "yes_no_unk_id"
    t.integer "relationship_id"
    t.integer "exposure_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ffpes", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "demographic_id"
    t.string "tma"
    t.string "tma_cores"
    t.string "core_location"
    t.string "path_response_slides"
    t.string "slide_location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "field_definitions", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "table_name"
    t.string "field_name"
    t.string "definition"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "final_mdt_stages", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "demographic_id"
    t.integer "staging_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "mets_location"
    t.string "tnm_t"
    t.string "tnm_n"
    t.string "tnm_m"
  end

  create_table "follow_ups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "demographic_id"
    t.date "dod"
    t.date "dls"
    t.date "date_of_recurrence"
    t.string "region_of_recurrence"
    t.integer "evidence_for_recurrence_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "in_hospital_death"
    t.string "recurrence"
    t.string "censor_value"
    t.string "local_recurrence"
  end

  create_table "genders", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gradings", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "histological_responses", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "histologies", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "intent_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "laparoscopies", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text "macroscopic_findings"
    t.date "date"
    t.integer "staging_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tumour_location"
    t.string "dmpd"
    t.string "ascites"
    t.string "mom"
    t.string "primary_tumour_extension"
    t.string "washings_report"
    t.string "metastasis_pathology"
  end

  create_table "length_of_uses", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "local_recurrences", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medications", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "exposure_id"
    t.integer "drug_id"
    t.integer "length_of_use_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "metastasis_pathologies", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "other_investigations", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "staging_id"
    t.text "report"
    t.integer "metastatsis_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pathologies", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "resection_pathology_number"
    t.float "maximum_tumour_diameter", limit: 24
    t.float "distance_to_proximal_margin", limit: 24
    t.float "distance_to_distal_margin", limit: 24
    t.float "distance_to_circumferential_resection_margin", limit: 24
    t.integer "number_of_nodes"
    t.integer "postive_nodes"
    t.string "location_of_involved_nodes"
    t.integer "demographic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "diagnostic_path_no"
    t.string "signet_ring_cells"
    t.string "tumour_location"
    t.string "histology"
    t.string "grade"
    t.string "resection"
    t.string "lymphovascalar_invasion"
    t.string "perineural_invasion"
    t.string "venous_invasion"
    t.string "tnm_t"
    t.string "tnm_n"
    t.string "tnm_m"
    t.string "nodes_on_both_sides"
    t.string "histological_response"
  end

  create_table "post_chemotherapy_restaging_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "post_chemotherapy_restagings", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "demographic_id"
    t.integer "staging_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pcr_type"
    t.string "finding"
  end

  create_table "presenting_complaint_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "presenting_complaints", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "exposure_id"
    t.integer "presenting_complaint_lookup_id"
  end

  create_table "previous_barrett_managements", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.integer "exposure_id"
    t.integer "barrett_managment_lookup_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "primary_tumour_extensions", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "radiotherapy_intent_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "referral_sources", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "relationships", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "resection_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "responses", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "samples", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "label"
    t.string "barcode"
    t.integer "demographic_id"
    t.text "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sample_class"
    t.string "sample_type"
    t.datetime "collection_time"
    t.datetime "frozen_time"
    t.string "haemolysed"
    t.string "event_position"
    t.string "location"
    t.string "section_undertaken"
    t.string "histology"
    t.string "path_response_slides"
    t.string "histology_text"
    t.string "on_tma"
    t.string "tma_cores"
    t.string "core_location"
  end

  create_table "sessions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "session_id", null: false
    t.string "cas_ticket"
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cas_ticket"], name: "index_sessions_on_cas_ticket"
    t.index ["session_id"], name: "index_sessions_on_session_id"
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "smokers", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "staging_ctpets", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text "report"
    t.integer "positive_node_location_id"
    t.integer "demographic_id"
    t.integer "staging_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tnm_t"
    t.string "tnm_n"
    t.string "tnm_m"
  end

  create_table "staging_cts", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text "report"
    t.integer "positive_nodes"
    t.integer "mets_location_id"
    t.integer "staging_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tnm_t"
    t.string "tnm_n"
    t.string "tnm_m"
  end

  create_table "staging_endoscopic_ultrasounds", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "demographic_id"
    t.text "report"
    t.integer "tumour_type_id"
    t.integer "mets_location_id"
    t.integer "fna_location_id"
    t.integer "staging_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "nodes"
    t.float "tumour_from", limit: 24
    t.float "tumour_to", limit: 24
    t.string "tnm_t"
    t.string "tnm_n"
    t.string "tnm_m"
  end

  create_table "stagings", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.date "date_of_diagnosis"
    t.integer "demographic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tumour_location"
    t.string "histology"
  end

  create_table "surgery_consultants", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "surgery_operations", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "phase"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tnm_ms", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tnm_ns", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tnm_ts", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "treatment_intent_lookups", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "treatments", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "demographic_id"
    t.integer "radio_dose"
    t.integer "radio_fractions"
    t.string "radio_complications"
    t.date "surgery_date"
    t.string "preop_chemo_complications"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "postop_chemo_commenced_date"
    t.string "preop_chemo_reg"
    t.string "postop_chemo_reg"
    t.string "treatment_intent"
    t.string "preop_chemo_type"
    t.string "radio_type"
    t.string "patient_received_deuterium"
    t.string "surgery_happened"
    t.string "surgery_consultant"
    t.string "surgery_operation"
    t.string "abdominal_phase"
    t.string "thoracic_phase"
    t.string "chemotherapy_why_off_lookup"
    t.string "post_op_intent"
  end

  create_table "tumour_locations", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tumour_types", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "username"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "washings_reports", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "yes_no_unknowns", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
