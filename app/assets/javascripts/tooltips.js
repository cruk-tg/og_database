$(function() {
    var initialText = 'Loading...';
    var tip;
    var table = 'exposure';

    function investigateLinks(elem) {
        var jElem = $('#' + elem);
        if ( jElem.parents('.table-name').length > 0) {
            return jElem.parents('.table-name').first().data('table-name');
        }
        return 0;
    }

    var onShow = function() {
        var content = $('.tippy-tooltip-content');
        var refElement  = tip.getReferenceElement(this);
        if ($(refElement).attr('data-original-title').localeCompare('Loading...') != 0)
            return;

        var inputId = $(refElement).children('input, select, textarea').first().attr('id');
        var nameArr = inputId.split('_').reverse();
        var table = null;
        var field = null;
        if (investigateLinks(inputId) == 0) {
            table = nameArr.pop();
            if (table == 'follow') {
                nameArr.pop(); //remove up
                table = 'follow_up';
            }
            nameArr = Sugar.Array(nameArr).exclude("attributes");
            field = nameArr.reverse().join('_').raw;
        } else {
            nameArr = nameArr.reverse();
            table =  investigateLinks(inputId);
            var index = nameArr.indexOf('attributes');
            if (index > 0) {
                field = nameArr.slice(index+1).join('_');
            } else {
                field = nameArr.join('_');
            }

        }

        if (tip.loading) return;
        tip.loading = true;
        $.get(Routes.definitions_show_path({table_name: table, field_name: field}))
            .done(function(data) {
                refElement.setAttribute('title', data[0]['definition']);
            })
            .fail(function() {
                refElement.setAttribute('title', 'FAIL');
            })
            .always(function() {
                tip.update(tip.getPopperElement(refElement));
                tip.loading = false;
            });
    };
    tip = tippy('.input-group', {
        arrowSize: 'big',
        arrow: true,
        onShow: onShow,
        theme: 'bigger'});
});
