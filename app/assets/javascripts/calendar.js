var ECMC = (function(my, $) {
    my.faDatetimePicker = function(control) {
        function init() {
            $(control).datetimepicker({
                locale: 'en-gb',
                format: 'DD/MM/YYYY',
                extraFormats: [ 'YYYY-MM-DD'],
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-exclamation",
                    clear: "fa fa-eraser",
                    close: "fa fa-close"
                }
            });
        }
        return init();
    };
    return my;
})(ECMC || {}, jQuery);
$(function() {
    ECMC.faDatetimePicker($('.input-group-addon > .fa-calendar').closest('.input-group'));
});
