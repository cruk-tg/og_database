# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
#
#disable_controls = () -> jQuery(class_name + " input").attr('disabled', true)
#jQuery ->
#        disable_controls(".know_barretts_values")
#        disable_controls(".smoker_values")
#        jQuery("#exposure_known_barretts_id").change ->
#             alert this.value
$ ->
        $(".known_barretts_values input").attr('disabled', true)
        $(".smoker_values input").attr('disabled', true)
        $("#exposure_bmi").attr('readonly', true)
        $("#exposure_smoker_pack_years").attr('readonly', true)
                
        $("#exposure_known_barretts").change ->
                if this.value == "Yes"
                        $(".known_barretts_values input").removeAttr('disabled')
                else
                        $(".known_barretts_values input").attr('disabled', true)

        $("#exposure_smoker").change ->
                if this.value == "Current" || this.value == "Ex-Smoker"
                        $(".smoker_values input").removeAttr('disabled')
                else
                        $(".smoker_values input").attr('disabled', true)

        bmi_function = () ->
                eh = $("#exposure_height")[0].value
                ew = $("#exposure_weight")[0].value

                bmi = ew/(eh*eh)
                bmi = Math.floor(bmi*100)
                $("#exposure_bmi")[0].value = bmi/100

        smoker_function = () ->
                years = $("#exposure_smoker_years")[0].value
                packs = $("#exposure_smoker_packs")[0].value

                $("#exposure_smoker_pack_years")[0].value = years * packs
                
        $("#exposure_height").change(bmi_function)
        $("#exposure_weight").change(bmi_function)
        $("#exposure_smoker_years").change(smoker_function)
        $("#exposure_smoker_packs").change(smoker_function)
                

