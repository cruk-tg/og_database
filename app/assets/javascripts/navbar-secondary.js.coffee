$ ->
        check_screen = (path) ->
                if URI().path().includes(path)
                        $('.navbar-secondary').find('.active').removeClass('active')
                        $("#nav-#{path}").addClass("active")

        check_screen path for path in ['demographic', 'exposure', 'staging', 'treatment', 'pathologies', 'follow_up', 'ffpe', 'samples']

        if $('#search_results_table').length > 0
                $('.navbar-secondary').on('click', 'a', ->
                        li_id = $(this).closest('li').attr('id')
                        form = $('#q').closest('form').get(0)
                        hiddenField = document.createElement("input")
                        hiddenField.setAttribute("type", "hidden")
                        hiddenField.setAttribute("name", "quickq")
                        hiddenField.setAttribute("value", li_id)

                        form.appendChild(hiddenField)

                        form.submit();
                        )
