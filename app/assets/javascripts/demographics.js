$(function() {
    $('#search_results_table')
        .dataTable({
            sPaginationType: "full_numbers",
            language: {
                search: "Search in table:"
            }
        });
});
