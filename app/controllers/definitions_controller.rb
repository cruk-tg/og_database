class DefinitionsController < ApplicationController
  def show
    definition = FieldDefinition.where selector
    if definition.any?
      render json: definition
    else
      render json: [selector.merge({:definition => params[:field_name]})]
    end
  end

  private
  def has_human_name?(table, field)

  end

  def selector
    if params[:field_name].include? "attributes"
      a = params[:field_name].split('_')
      b = a.index("attributes") - 1
      {:table_name => params[:table_name], :field_name => a[0..b].join('_')}
    else
      params.permit(:table_name, :field_name)
    end
  end
end
