class SamplesController < StandardFormsController
  def index
    @demographic = Demographic.find_by_id(params[:demographic_id])
    @samples = @demographic.samples.decorate.reject { |s| s.storage_location.nil? }
  end

  def edit
  end

  def new
  end

  def update
    params["sample"] = params["sample"].delete_if { |the_key| Sample.ca_fields.include? the_key }
    super()
  end
  def get_by_id
    super()
    @form_model = @form_model.decorate if @form_model.respond_to? :decorate
  end


end
