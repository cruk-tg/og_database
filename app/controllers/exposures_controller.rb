class ExposuresController < StandardFormsController
  def create
    params[:exposure][:bmi] = (params[:exposure][:weight].to_f / (params[:exposure][:height].to_f**2)*100).floor.to_f/100  unless params[:exposure][:weight].empty? || params[:exposure][:height].empty?
    Exposure.transaction do
      super()
      save_nested
    end
  end

  def update
    Exposure.transaction do
      super()
      save_nested
    end
  end
  def form_attrs
    params.require(:exposure).permit(fields)
  end

  def fields
    %w(alcohol previous_alcohol_excess known_barretts barretts_length prague_classification_c prague_classification_m pulmonary_function_fev1_lm pulmonary_function_fev1_p pulmonary_function_fvc_lm pulmonary_function_fvc_p pulmonary_function_fev1_fvc_p height weight bmo smoker smoker_packs smoker_years smoker_pack_years egfr albumin haemoglobin weight_loss weight_loss_weeks exercise_tolerance asa_grade referral_source)
  end

  private

  def save_nested
    if @form_model.errors.empty? && medications
      medications.each do |k,v|
        next if v[:drug_id].nil? || v[:drug_id].empty?
        if (v.has_key?(:id))
          medication = Medication.find(v[:id])
        else
          medication = Medication.new
        end
        medication.drug = Drug.find(v[:drug_id])
        medication.length_of_use = LengthOfUse.find(v[:length_of_use_id])
        medication.exposure = @form_model
        medication.save!
      end
    end
    if @form_model.errors.empty?
      family_og.each do |k,v|
        next if v[:relationship_id].nil? || v[:relationship_id].empty?
        if (v.has_key?(:id))
          f = FamilyHistoryOfOgCancer.find(v[:id])
        else
          f = FamilyHistoryOfOgCancer.new
        end
        f.relationship = Relationship.find(v[:relationship_id])
        f.exposure = @form_model
        f.save!
      end
      family_other.each do |k,v|
        next if v[:relationship_id].nil? || v[:relationship_id].empty?
        if (v.has_key?(:id))
          f = FamilyHistoryOfOtherCancer.find(v[:id])
        else
          f = FamilyHistoryOfOtherCancer.new
        end
        f.relationship = Relationship.find(v[:relationship_id])
        f.exposure = @form_model
        f.save!
      end
    end
    if @form_model.errors.empty? && presenting_complaint
      PresentingComplaint.where(exposure_id: @form_model.id).delete_all
      presenting_complaint.each do |i|
        PresentingComplaint.create! exposure_id: @form_model.id, presenting_complaint_lookup_id: i.to_i
      end
    end
    if @form_model.errors.empty? && co_morbidities
      CoMorbidity.where(exposure_id: @form_model.id).delete_all
      co_morbidities.each do |i|
        CoMorbidity.create! exposure_id: @form_model.id, co_morbidity_lookup_id: i.to_i
      end
    end
  end

  def family_og
    dig_or_empty(:exposure, :family_history_of_og_cancer_attributes)
  end

  def family_other
    dig_or_empty(:exposure, :family_history_of_other_cancer_attributes)
  end

  def medications
    params.dig(:exposure, :medications_attributes)
  end

  def presenting_complaint
    params.dig(:exposure, :presenting_complaint_lookup_ids)
  end

  def co_morbidities
    params.dig(:exposure, :co_morbidity_lookup_ids)
  end
end
