class PathologiesController < StandardFormsController
  def new
  end

  def create
    Pathology.transaction do
      super()
      save_nested
    end
  end
  def edit
  end

  def update
    Pathology.transaction do
      super()
      save_nested
    end
  end

  def form_attrs
    params.require(:pathology).permit(fields)
  end

  def fields
    ["resection_pathology_number", "maximum_tumour_diameter", "distance_to_proximal_margin", "distance_to_distal_margin", "distance_to_circumferential_resection_margin", "number_of_nodes", "postive_nodes", "location_of_involved_nodes", "diagnostic_path_no", "signet_ring_cells", "tumour_location", "histology", "grade", "resection", "lymphovascalar_invasion", "perineural_invasion", "venous_invasion", "tnm_t", "tnm_n", "tnm_m", "nodes_on_both_sides", "histological_response"]
  end
  private
  def save_nested
    if @form_model.errors.empty? && adjacent_information.any?
      BarrettsAdjacentToTumour.where(pathology_id: @form_model.id).delete_all
      adjacent_information.each do |i|
        BarrettsAdjacentToTumour.create! pathology_id: @form_model.id, barretts_adjacent_lookup_id: i.to_i
      end
    end
  end

  def adjacent_information
    a = params['pathology']['barretts_adjacent_lookup_ids'] || []
    a.reject(&:empty?)
  end
end
