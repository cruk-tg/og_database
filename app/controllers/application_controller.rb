class ApplicationController < ActionController::Base
  #TODO sort authorised and authenticated
  # This has a good method for doing this, port to Sammansys
  before_action :set_notification

  class NotAuthenticated < StandardError
  end

  class NoLogin < StandardError
  end

  protect_from_forgery
  helper_method :admin?

  rescue_from NotAuthenticated, with: :not_authenticated
  rescue_from NoLogin, with: :no_login

  before_action :check_authentication
  def logout
    redirect_to 'https://www.ecmc.ed.ac.uk/cas/logout'
  end

  def admin?
    true
  end

  helper_method :current_user
  helper_method :errors
  helper_method :js_loglevel

  def current_user
    OpenStruct.new(:id => cas_user, :name => display_name)
  end

  def errors
    []
  end

  def errors_to_json
    { :errors => errors.messages, :messages => errors.full_messages }
  end

  def js_loglevel
    if Rails.env.development?
      "DEBUG"
    else
      "ERROR"
    end
  end

  protected
  def set_notification
    request.env['exception_notifier.exception_data'] = { 'username' => display_name}
  end

  def check_authentication
    render plain: "Not authenticated", status: :unauthorized unless authorized?
  end

  def authorized?
    !User.find_by_username(cas_user).nil?
  end

  def display_name
    session["cas"]["extra_attributes"]["displayName"] if session["cas"] && session["cas"]["extra_attributes"]
  end

  def cas_user
    session.has_key?("cas") ? session["cas"]["user"] : false
  end

  def error_reporter(exception)
    if (Rails.env.development?)
      errors = exception.backtrace
      message = "#{exception.class} #{exception.message}"
    else
      errors = []
      message = "An internal error occurred"
    end
    render json: { errors: errors, messages: message }, status: 500
  end

    #  def check_authentication
    #   raise NotAuthenticated unless session['cas'] && session['cas']['user']
    #   raise NoLogin unless User.find_by_username(session['cas']['user'])
    # end

    def not_authenticated
      render text: "Not authenticated", status: :unauthorized
    end

    def no_login
      render text: "Forbidden", status: :forbidden
    end

    def dig_or_empty(*args)
      params.dig(*args) || []
    end
end
