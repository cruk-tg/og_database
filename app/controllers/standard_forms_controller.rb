class StandardFormsController < ApplicationController
  before_action :get_by_id, :only => [:edit, :update, :show]
  before_action :get_new, :only => [:new, :create, :index]

  helper_method :form_model
  helper_method :demographic

  def demographic
    @form_model.persisted? ? @form_model.demographic : Demographic.find(params[:demographic_id])
  end

  def form_model
    if @form_model.persisted?
      @form_model
    else
      [Demographic.find(params[:demographic_id]),@form_model]
    end
  end

  def create
    respond_to do |format|
      @form_model.demographic_id = params[:demographic_id] if params[:demographic_id]
      if @form_model.update_attributes(form_attrs)
        format.html { redirect_to(url_for([:edit, @form_model]), :notice => t(:successful_form_model_create, :model => form_model_class.name.humanize))}
      else
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @form_model.update_attributes(form_attrs)
        format.html { redirect_to(url_for([:edit,@form_model]), :notice => t(:successful_form_model_update, :model => form_model_class.name.humanize))}
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def form_models
    @form_models = form_model_class.find(:all)
  end

  def form_model_class
    Object.class_eval(self.class.name.gsub("Controller","").singularize)
  end

  protected

  def form_model=(form_model)
    @form_model=form_model
  end

  def get_by_id
    @form_model = form_model_class.find(params[:id]) if @form_model.nil?
  end

  def get_new
    @form_model = form_model_class.new
  end

  def form_attrs
    params.require(form_model_class.name.underscore.to_sym).permit!
  end
end
