class StagingsController < StandardFormsController
  def build_sub_forms(forms)
    forms.each do |form|
      @form_model.public_send("build_#{form}") unless @form_model.public_send("#{form}")
    end
  end

  def new
    build_sub_forms(%w(staging_ct staging_ctpet staging_endoscopic_ultrasound other_investigation laparoscopy post_chemotherapy_restaging final_mdt_stage))
  end

  def edit
  end
end
