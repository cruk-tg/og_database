class FollowUpsController < StandardFormsController
  def new
  end

  def create
    FollowUp.transaction do
      super()
      save_nested
    end
  end

  def edit
  end

  def update
    FollowUp.transaction do
      super()
      save_nested
    end
  end

  def form_attrs
    params.require(:follow_up).permit(fields)
  end

  def fields
    %w(censor_value dod dls in_hospital_death recurrence date_of_recurrence region_of_recurrence local_recurrence)
  end

  private
  def save_nested
    if @form_model.errors.empty? && evidence_for_recurrence.any?
      EvidenceForRecurrence.where(:follow_up_id => @form_model.id).delete_all
      evidence_for_recurrence.each do |i|
        EvidenceForRecurrence.create! follow_up_id: @form_model.id, evidence_for_recurrence_lookup_id: i.to_i
      end
    end
  end

  def evidence_for_recurrence
    a  = params['follow_up']['evidence_for_recurrence_lookup_ids'] || []
    a.reject(&:empty?)
  end
end
