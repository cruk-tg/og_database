class DemographicsController < ApplicationController
  before_action :get_new, :only => [:new, :create, :search, :recurrences]
  before_action :get_by_id, :only => [:edit, :update, :show]

  helper_method :recurrences
  helper_method :search_results
  helper_method :reporting
  helper_method :demographic

  def get_new
    @demographic = Demographic.new
  end

  def get_by_id
    @demographic = Demographic.find params[:id]
  end

  def demographic
    @demographic
  end

  def new
  end

  def create
    if @demographic.update_attributes(form_attrs)
      redirect_to edit_demographic_url(@demographic), :notice => t(:successful_form_model_create, :model => Demographic.name.humanize)
    else
      render :action => "new"
    end
  end

  def edit
  end

  def update
    if @demographic.update_attributes(form_attrs)
      redirect_to edit_demographic_url(@demographic), :notice => t(:successful_form_model_update, :model => Demographic.name.humanize)
    else
      render :action => "edit"
    end
  end

  def form_attrs
    params.require(:demographic).permit(fields)
  end

  def fields
    %w(chi hospital_number oe_number occams_id neo_aegis gp_name gp_address1 gp_address2 gp_address3 gp_address4 gp_phone_number date_of_ecmc_consent date_of_occams_consent gender_id)
  end

  # searching below
  def search_results
    search
  end

  def search
    if request.method == "POST"
      @search_results = get_search_results
    else
      @search_results = Demographic.all.includes(:follow_up, :treatment)
    end
  end

  def recurrences
    if params.has_key? :days
      @recurrences ||= Demographic.recurrence_happend_in params[:days]
    else
      @recurrences ||= Demographic.recurrence_free
    end
  end

  def reporting
    @reporting
  end

  def reports
    @demographics = DemographicDecorator.decorate_collection(Demographic.order(:oe_number).includes( :exposure, :treatment, :pathology, :ffpe, :follow_up => [:evidence_for_recurrences], :staging => [:staging_ct, :staging_ctpet, :staging_endoscopic_ultrasound, :laparoscopy, :other_investigation, :final_mdt_stage, :post_chemotherapy_restaging]))
    render :xlsx => 'report'
  end

  private
  def search_guard(k,v)
    k
  end

  def get_search_results
    if request.method == "POST"
      if (params.has_key? :quickq)
        quick_search(params[:quickq])
      elsif (params.has_key? :q)
        Demographic.joins(:pathology).where("pathologies.resection_pathology_number LIKE :this OR chi LIKE :this OR oe_number LIKE :this OR occams_id LIKE :this", this: "%#{params[:q]}%")
      else
        Demographic.recurrence_free
      end
    end
  end

  private

  def quick_search(event)
    case event
    when "navbar-post-op"
      Demographic.joins(:treatment).where("treatments.surgery_happened = 'Yes' AND treatments.post_op_intent = 'Adjuvant'")
    when "navbar-pre-op"
      Demographic.joins("LEFT JOIN treatments ON treatments.demographic_id = demographics.id").where("treatments.surgery_happened IS NULL")
    when "navbar-recurrence-free"
      Demographic.recurrence_free
    when "navbar-pathology-absent"
      Demographic.joins(:treatment)
        .includes(:pathology)
        .references(:pathology)
        .where("treatments.surgery_happened = 'Yes' AND (pathologies.diagnostic_path_no IS NULL OR pathologies.diagnostic_path_no = '') AND (pathologies.resection_pathology_number IS NULL OR pathologies.resection_pathology_number = '')")
    end
  end
end
