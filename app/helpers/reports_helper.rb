module ReportsHelper
  def titles
    array = Demographic.new.decorate.xls_fields.collect { |s| Demographic.human_attribute_name s}
    array += Exposure.new.decorate.xls_fields.collect { |s| Exposure.human_attribute_name s }
    array += Staging.new.decorate.xls_fields.collect { |s| Staging.human_attribute_name s }
    array += Treatment.new.decorate.xls_fields.collect { |s| Treatment.human_attribute_name s }
    array += Pathology.new.decorate.xls_fields.collect { |s| Pathology.human_attribute_name s }
    array += FollowUp.new.decorate.xls_fields.collect  { |s| FollowUp.human_attribute_name s }
    array += Ffpe.new.decorate.xls_fields.collect { |s| Ffpe.human_attribute_name s }
  end
end
