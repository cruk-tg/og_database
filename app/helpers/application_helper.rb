module ApplicationHelper
  def link_to_new_or_edit(class_name)
    if demographic.public_send(class_name).nil?
      link_to "#{t(:new_record)} #{t(class_name)}", public_send("new_demographic_#{class_name.to_s}_path",demographic)
    else
      link_to t(class_name), url_for([:edit,demographic.public_send(class_name)])
    end
  end

  def link_to_samples
    if demographic.samples.nil?
      link_to "#{t(:new_record)} #{t(:samples)}", new_demographic_sample_path(demographic)
    else
      link_to "#{t(:index_samples)}", demographic_samples_path(demographic)
    end
  end

  def link_to_demographic
    link_to t(:demographic), url_for([:edit, demographic]) if demographic.persisted?
  end

  def flash_message
    flash[:error] ||= flash[:notice]
  end

  def ecmc_form_for(record, options = {}, &block)
    options[:builder] ||= FormBuilders::BootstrapFormBuilder
    form_for(record, options, &block)
  end
end
