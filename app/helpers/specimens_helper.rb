module SpecimensHelper
  def fetch_samples_hash(data)
    puts data[:object]
    if (data[:object] == "SpecimenCollectionGroup")
      fetch_samples data[:data][:specimens]
    elsif (data[:object] == "Specimen")
      puts data[:data]
    else
      puts "data[:object] is unknown"
    end
  end

  def fetch_samples(data)
    puts data.class
    if (data.is_a? Array)
      data.each { |d| fetch_samples d }
    elsif (data.is_a? Hash)
      fetch_samples_hash data
    else
      {:error => 'Unknown data structure'}
    end
  end

end
