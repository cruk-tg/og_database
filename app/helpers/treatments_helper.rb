module TreatmentsHelper
  def surgery_phases_options(opt)
    SurgeryOperation.where(:phase => opt).map(&:name)
  end
end
