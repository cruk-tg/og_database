class EvidenceForRecurrence < ApplicationRecord
  belongs_to :follow_up
  belongs_to :evidence_for_recurrence_lookup
end
