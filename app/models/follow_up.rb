class FollowUp < ApplicationRecord
  belongs_to :demographic

  has_many :evidence_for_recurrences
  has_many :evidence_for_recurrence_lookups, :through => :evidence_for_recurrences

  audited

  validates_with Validators::FollowUpValidator

  def overall_survival_months
    0
  end
end
