class Treatment < ApplicationRecord
  belongs_to :demographic

  audited
end
