class PresentingComplaint < ApplicationRecord
  belongs_to :exposure
  belongs_to :presenting_complaint_lookup
end
