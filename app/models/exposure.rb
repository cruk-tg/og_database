class Exposure < ApplicationRecord
  belongs_to :demographic
  validates :demographic, :presence => true


#  has_many :barrett_managements_lookups, :through => :previous_barrett_managments
  has_many :presenting_complaints
  has_many :presenting_complaint_lookups, :through => :presenting_complaints

  #FIXME has_many without plural, is this correct?
  has_many :family_history_of_og_cancer
  accepts_nested_attributes_for :family_history_of_og_cancer

  has_many :family_history_of_other_cancer
  accepts_nested_attributes_for :family_history_of_other_cancer

  has_many :co_morbidities
  has_many :co_morbidity_lookups, :through => :co_morbidities

  has_many :medications
  has_many :drugs, :through => :medications
  accepts_nested_attributes_for :medications

  audited

  def bmi
      (100*weight/height**2).floor.to_f / 100 unless weight.nil? || height.nil?
  end
end
