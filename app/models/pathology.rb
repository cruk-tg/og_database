class Pathology < ApplicationRecord
  belongs_to :demographic

  has_many :barretts_adjacent_to_tumours
  has_many :barretts_adjacent_lookups, :through => :barretts_adjacent_to_tumours

  after_initialize :set_defaults

  audited

  def set_defaults
    self.resection_pathology_number ||= "UB" if self.respond_to?(:resection_pathology_number)
  end

end
