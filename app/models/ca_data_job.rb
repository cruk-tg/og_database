class CaDataJob < ApplicationRecord
  def self.running?
    CaDataJob.where(:status => :in_progress).any?
  end

  def self.last_successful
    CaDataJob.where(:status => :successful).order(:started).last
  end

  def self.started
    CaDataJob.create started: Time.now, status: :in_progress
  end

  def mark_complete
    self['status'] = :successful
    save
    self
  end

  def status
    self['status'].to_sym
  end
end
