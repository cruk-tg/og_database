class FamilyHistoryOfOtherCancer < ApplicationRecord
  belongs_to :exposure
  belongs_to :relationship
  validates :exposure, :relationship, presence: true
end
