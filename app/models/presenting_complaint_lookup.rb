class PresentingComplaintLookup < ApplicationRecord
  has_many :presenting_complaints
  has_many :exposures, :through => :presenting_complaints
end
