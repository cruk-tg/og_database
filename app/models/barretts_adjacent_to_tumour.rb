class BarrettsAdjacentToTumour < ApplicationRecord
  belongs_to :pathology
  belongs_to :barretts_adjacent_lookup
end
