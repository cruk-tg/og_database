class CoMorbidityLookup < ApplicationRecord
  has_many :co_morbidities
  has_many :exposures, :through => :co_morbidities
end
