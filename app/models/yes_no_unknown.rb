class YesNoUnknown < ApplicationRecord
  def is_yes?
    self.name.downcase == "yes"
  end

  def is_no?
    self.name.downcase == "no"
  end

  def is_unknown?
    self.name.downcase == "unknown"
  end
end
