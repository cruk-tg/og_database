class Specimen
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  def persisted?
    false
  end

  def self.find_by_participant_id(participant_id)

  end

  def demographic
    #FIXME get demograph from data
    Demographic.all.first
  end

  def label
    @label
  end

  def barcode
    @barcode
  end

  def comment
    @comment
  end
  
  def collected_at
    @collected_at
  end

  def received_at
    @received_at
  end

  private
  def self.create_from_local_db(participant_id)
    Sample
  end

  def self.create_from_remote_db(participant_id)
    @label = data[:label]
    @barcode = data[:barcode]
    if data.has_key? :specimen_events
      data[:specimen_events].each do |event|
        if event[:object] == "CollectionEventParameters"
          @collected_at = event[:data][:time]
        end
        if event[:object] == "ReceivedEventParameters"
          @received_at = event[:data][:time]
        end
      end
    end

  end

  def initialize(data)
  end

end
