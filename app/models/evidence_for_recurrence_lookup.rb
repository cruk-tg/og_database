class EvidenceForRecurrenceLookup < ApplicationRecord
  has_many :evidence_for_recurrences
  has_many :pathologies, :through => :evidence_for_recurrences
end
