class Demographic < ApplicationRecord
  belongs_to :gender

  validates :chi, :presence => true, :uniqueness => { :case_sensitive => false }
  validates :occams_id, :uniqueness => { :case_sensitive => false }, :allow_blank => true
  validates :oe_number,  :presence => true, :uniqueness => { :case_sensitive => false }
  validates :neo_aegis, :uniqueness => { :case_sensitive => false }, :allow_blank => true
  validates :date_of_ecmc_consent, :presence => true
  validates :date_of_occams_consent, :presence => true

  validates_with Validators::DemographicIdsValidator

  after_initialize :set_defaults

  has_one :exposure
  has_one :staging
  has_one :treatment
  has_one :pathology
  has_one :follow_up
  has_one :ffpe
  has_many :samples
  audited

  DEFAULTS = { :chi => '', :oe_number => 'OC', :occams_id => 'OCCAMS/ED' }


  def demographic
    self
  end

  def set_defaults
    Demographic::DEFAULTS.each { |k,v| self.public_send("#{k}=",v) if self.public_send("#{k}").nil? }
  end

  def self.recurrence_happend_in(days)
    valid_records = Demographic.all.select { |d| !d.follow_up.nil? && !d.follow_up.date_of_recurrence.nil? && !d.treatment.nil? && !d.treatment.surgery_date.nil? }
    valid_records.select { |d| (d.follow_up.date_of_recurrence - d.treatment.surgery_date) < days.to_i }
  end

  def self.recurrence_free
    Demographic.includes(:follow_up).references(:follow_up).where("follow_ups.date_of_recurrence IS NULL")
  end

  def gender
    case self[:gender_id]
    when 1
      "Male"
    when 2
      "Female"
    when 3
      "Other/NOS"
    else
      "NOS"
    end
  end
end
