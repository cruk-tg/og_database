class Medication < ApplicationRecord
  belongs_to :exposure
  belongs_to :drug
  belongs_to :length_of_use

  validates :exposure, :drug, presence: true
end
