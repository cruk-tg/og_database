class OtherInvestigation < ApplicationRecord
  belongs_to :staging
  belongs_to :metastatsis, :class_name => 'YesNoUnknown'

  def metastatsis_text
    'metastatsis_text'
  end
end
