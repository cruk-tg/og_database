class Staging < ApplicationRecord
  belongs_to :demographic

  has_one :staging_ct
  accepts_nested_attributes_for :staging_ct

  has_one :staging_ctpet
  accepts_nested_attributes_for :staging_ctpet

  has_one :staging_endoscopic_ultrasound
  accepts_nested_attributes_for :staging_endoscopic_ultrasound

  has_one :post_chemotherapy_restaging
  accepts_nested_attributes_for :post_chemotherapy_restaging

  has_one :laparoscopy
  accepts_nested_attributes_for :laparoscopy

  has_one :other_investigation
  accepts_nested_attributes_for :other_investigation

  has_one :final_mdt_stage
  accepts_nested_attributes_for :final_mdt_stage

  audited

  def method_missing(method_name, *arguments, &block)
    if method_test(method_name.to_s)
      Staging.has_one_tables.each do |name|
        if method_name.to_s.starts_with?(name)
          local_method = method_name.to_s.sub("#{name}_", '')
          if self.public_send(name).nil?
            return ''
          else
            return self.public_send(name).public_send(local_method)
          end
        end
      end
      super
    end
  end

  def respond_to_missing?(method_name, include_private = false)
    super || method_test(method_name.to_s)
  end

  private
  def method_test(method_name)
    Staging.has_one_tables.each do |name|
      if method_name.starts_with?(name)
        local_method = method_name.sub("#{name}_",'')
        const = name.camelize.constantize
        return const.new.respond_to? local_method
      end
    end
    false
  end

  def self.has_one_tables
    selector = -> (assoc) { assoc.is_a? ActiveRecord::Reflection::HasOneReflection}
    Staging.reflect_on_all_associations
      .select(&selector)
      .collect(&:name)
      .collect(&:to_s)
      .sort { |a,b| b.length <=> a.length }
  end
end
