class Sample < ApplicationRecord
  belongs_to :demographic

  def extra
    if self[:extra].nil? || self[:extra] == ""
      Hash.new
    else
      YAML.load(self[:extra])
    end
  end

  def extra=(v)
      self[:extra] = v.to_yaml
  end

  def storage_location
    if has_storage_location?
      extra[:storage_location].gsub('In Transit Container', '').strip
    end
  end

  def interval
    if self.collection_time && self.frozen_time
      (self.frozen_time - self.collection_time).to_i / 60
    else
      nil
    end
  end

  def self.ca_fields
    %w(label barcode sample_class sample_type collection_time frozen_time storage_location)
  end

  def has_storage_location?
    extra.present? && extra.key?(:storage_location) && extra[:storage_location].is_a?(String)
  end
end
