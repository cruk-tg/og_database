class CoMorbidity < ApplicationRecord
  belongs_to :exposure
  belongs_to :co_morbidity_lookup
end
