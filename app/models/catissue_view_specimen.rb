class CatissueViewSpecimen < CatissueDatabase
  self.primary_key    = "IDENTIFIER".downcase # CaTissue Default ID
  self.table_name = "catissue_view_specimens"

  belongs_to :catissue_view_specimen_coll_group

  default_scope { where("label LIKE 'OE%'") }
  scope :collected, -> { where(:collection_status => :collected) }

  def specimen_collection_group
    catissue_view_specimen_coll_group
  end

  def to_hash
    {
                  id: self.identifier,
               label: self.label,
             barcode: self.barcode,
           available: self.available,
            comments: self.comments,
            quantity: {
                  available: self.available_quantity,
                    initial: self.initial_quantity
                      },
   collection_status: self.collection_status,
      specimen_class: self.specimen_class,
       specimen_type: self.specimen_type,
             lineage: self.lineage,
 pathological_status: self.pathological_status,
    storage_location: self.storage_location,
         frozen_time: self.frozen_time,
      collected_time: self.collected_time
    }
  end

end
