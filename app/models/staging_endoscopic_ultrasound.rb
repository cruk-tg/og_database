class StagingEndoscopicUltrasound < ApplicationRecord
  include Reporting::XlsFields

  belongs_to :demographic
  belongs_to :staging

  def extra_fields
    {}
  end
end
