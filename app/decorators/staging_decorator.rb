class StagingDecorator < Draper::Decorator
  include Reporting::XlsFields
  include Reporting::XlsData
  delegate_all

  def extra_fields
    []
  end


  def ordered_fields
    [:date_of_diagnosis, :tumour_location, :histology, :staging_ct_report, :staging_ct_positive_nodes, :staging_ct_tnm_t, :staging_ct_tnm_n, :staging_ct_tnm_m, :staging_ctpet_report, :staging_ctpet_tnm_t, :staging_ctpet_tnm_n, :staging_ctpet_tnm_m, :staging_endoscopic_ultrasound_report, :staging_endoscopic_ultrasound_tumour_from, :staging_endoscopic_ultrasound_tumour_to, :staging_endoscopic_ultrasound_nodes, :staging_endoscopic_ultrasound_tnm_t, :staging_endoscopic_ultrasound_tnm_n, :staging_endoscopic_ultrasound_tnm_m, :post_chemotherapy_restaging_pcr_type, :laparoscopy_macroscopic_findings, :laparoscopy_date, :laparoscopy_tumour_location, :laparoscopy_dmpd, :laparoscopy_ascites, :laparoscopy_mom, :laparoscopy_primary_tumour_extension, :laparoscopy_washings_report, :laparoscopy_metastasis_pathology, :other_investigation_report, :other_investigation_metastatsis_text, :final_mdt_stage_tnm_t, :final_mdt_stage_tnm_n, :final_mdt_stage_tnm_m, :final_mdt_stage_mets_location]
  end
end
