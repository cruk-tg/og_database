class ExposureDecorator < Draper::Decorator
  include Reporting::XlsFields
  include Reporting::XlsData
  delegate_all

  def ordered_fields
    [:known_barretts, :barretts_length, :prague_classification_c, :prague_classification_m, :previous_alcohol_excess, :alcohol, :pulmonary_function_fev1_lm, :pulmonary_function_fev1_p, :pulmonary_function_fvc_lm, :pulmonary_function_fvc_p, :pulmonary_function_fev1_fvc_p, :egfr, :albumin, :haemoglobin, :weight_loss, :weight_loss_weeks, :height, :weight, :bmi, :smoker, :smoker_years, :smoker_packs, :smoker_pack_years, :exercise_tolerance, :asa_grade, :referral_source, :presenting_complaints, :co_morbidities, :drugs, :family_history_of_og_cancer, :family_history_of_other_cancer]
  end

  def extra_fields
    []
  end

  def family_history_of_og_cancer_list
    self.family_history_of_og_cancer.collect { |fh| fh.relationship.name unless fh.relationship.nil? }.join(',')
  end

  def family_history_of_other_cancer_list
    self.family_history_of_other_cancer.collect { |fh| fh.relationship.name unless fh.relationship.nil?}.join(',')
  end

  def presenting_complaints_list
    self.presenting_complaints.collect { |pc| pc.presenting_complaint_lookup.name unless pc.presenting_complaint_lookup.nil? }.join(',')
  end

  def co_morbidities_list
    self.co_morbidities.collect { |co| co.co_morbidity_lookup.name unless co.co_morbidity_lookup.nil? }.join(',')
  end

  def drugs_list
    self.medications.collect { |med| "#{med.drug.name} (#{med.length_of_use ? med.length_of_use.name : ""})"  unless med.drug.nil? }.join(',')
  end


end
