# -*- coding: utf-8 -*-
class SampleDecorator < Draper::Decorator
  delegate_all

  decorates_association :demographic

  def sample_block
    readonly_fields.inject("") { |str,field| str += field_to_text(field) }
  end

  def field_to_text(field)
    h.content_tag(:b,field.to_s).concat(object.public_send("#{field}"))
  end

  def field_line(field)
    field_to_label(field).concat(field_to_input(field))
  end

  def field_to_label(field)
    h.content_tag(:label, field.to_s)
  end

  def field_to_input(field)
    h.text_field(:specimen, field)
  end

  def sample_class_list
    %w(Tissue Fluid Cell Molecular)
  end

  def readonly_fields
    [:label,:barcode,:sample_class,:sample_type]
  end

  def rw_fields
    object.class.fields - readonly_fields
  end

  def each_readwrite_field(&block)
    rw_fields.each do |field|
      case block.arity
      when 0
        yield field_line(field)
      when 1
        hsh = {:label => field_to_label(field), :input => field_to_input(field)}
        yield hsh
      when 2
        yield field_to_label(field), field_to_input(field)
      end
    end
  end

  def edit_button
    h.button_to("Edit", h.edit_sample_path(object), :method => "get")
  end

  def histology_list
    ["Pending", "Other", "Normal Squamous", "Inflamed Normal Squamous", "Barrett's-IM", "Barrett's-LGD", "Barrett'’s-HGD", "Intramucosal Adenocarcinoma", "Adenocarcinoma", "Adenocarcinoma with overlying Normal Squamous", "Adenocarcinoma with overlying Normal Gastric", "Squamous-LGD", "Squamous-HGD", "Squamous cell carcinoma", "Normal Gastric", "Inflamed Gastric", "Atrophic Gastric", "Gastric metaplasia", "Non-diagnostic"]
  end

  def xls_fields
    [sample.demographic.oe_number, sample.label, sample.barcode,
     sample.sample_class, sample.sample_type, sample.collection_time,
     sample.frozen_time, sample.haemolysed, sample.event_position,
     sample.histology, sample.histology_text]
  end
end
