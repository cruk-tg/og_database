class FfpeDecorator < Draper::Decorator
  include Reporting::XlsFields
  include Reporting::XlsData
  delegate_all

  def extra_fields
    {}
  end

  def ordered_fields
    [:tma]
  end
end
