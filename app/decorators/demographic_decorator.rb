require "builder"
class DemographicDecorator < Draper::Decorator
  include Reporting::XlsFields
  include Reporting::XlsData

  delegate_all

  decorates_association :exposure
  decorates_association :staging
  decorates_association :treatment
  decorates_association :pathology
  decorates_association :follow_up
  decorates_association :ffpe

  def extra_fields
    [:occams_id]
  end

  def ordered_fields
    [:oe_number, :occams_id, :gender]
  end

  def get_data(table)
    if (self.public_send(table).nil?)
      get_empty(table)
    else
      self.public_send(table).xls_data
    end
  end

  def get_empty(table)
    table.to_s.camelize.constantize.new.decorate.xls_data
  end
end
