class TreatmentDecorator < Draper::Decorator
  include Reporting::XlsFields
  include Reporting::XlsData
  delegate_all

  def ordered_fields
    [:treatment_intent, :preop_chemo_type, :preop_chemo_reg,
     :preop_chemo_complications, :chemotherapy_why_off_lookup,
     :surgery_date, :surgery_happened, :surgery_consultant, :surgery_operation,
     :abdominal_phase, :thoracic_phase, :radio_type, :radio_dose, :radio_fractions,
     :radio_complications, :postop_chemo_commenced_date, :postop_chemo_reg,
     :patient_received_deuterium
    ]
  end

  def extra_fields
    []
  end

end
