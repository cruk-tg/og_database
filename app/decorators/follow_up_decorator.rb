class FollowUpDecorator < Draper::Decorator
  include Reporting::XlsFields
  include Reporting::XlsData
  delegate_all

  def extra_fields
    []
  end

  def ordered_fields
    [:censor_value, :dod, :dls, :overall_survival_months, :in_hospital_death, :evidence_for_recurrences]
  end

  def evidence_for_recurrences_list
    self.evidence_for_recurrences.collect { |efr| efr.evidence_for_recurrence_lookup.name unless efr.evidence_for_recurrence_lookup.nil? }.join(',')
  end

end
