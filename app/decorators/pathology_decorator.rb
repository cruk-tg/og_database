class PathologyDecorator < Draper::Decorator
  include Reporting::XlsFields
  include Reporting::XlsData
  delegate_all

  def extra_fields
    []
  end

  def ordered_fields
    [:resection_pathology_number, :diagnostic_path_no, :maximum_tumour_diameter, :distance_to_proximal_margin,	:distance_to_distal_margin, :distance_to_circumferential_resection_margin, :number_of_nodes, :postive_nodes, :location_of_involved_nodes, :tumour_location, :histology, :signet_ring_cells, :grade, :barretts_adjacent_to_tumours]
  end

  def barretts_adjacent_to_tumours_list
    self.barretts_adjacent_to_tumours.collect { |batt| batt.barretts_adjacent_lookup.name unless batt.barretts_adjacent_lookup.nil? }.join(',')
  end

end
