module Migration
  module Flatten
    def flatten(tablename, belongs_to)
      @tablename = tablename
      @belongs_to = belongs_to
      @key_value_pairs = Hash.new
      @belongs_to.keys.each do |field|
        @key_value_pairs[field] = Hash[@belongs_to[field].all.map { |h| [h.id,h.name] }]
        add_column @tablename, temp_field_name(field), :string
      end
      alter
      @belongs_to.keys.each do |field|
        remove_column @tablename, id_field_name(field)
        rename_column @tablename, temp_field_name(field), field
      end
    end

    def alter
      @tablename.to_s.classify.constantize.all.each do |e|
        @belongs_to.keys.each do |field|
          alter_field e, field, @key_value_pairs[field]
        end
        e.without_auditing do
          e.save!
        end
      end
    end

    def alter_field(record, field, values)
      if record.public_send(id_field_name(field))
        record.public_send("#{temp_field_name(field)}=",
                           values[record.public_send(id_field_name(field))])
      end
    end

    def temp_field_name(field)
      field.to_s.concat("_text")
    end

    def id_field_name(field)
      field.to_s.concat("_id")
    end
  end
end
