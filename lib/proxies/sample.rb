module Proxies
  class Sample
    include Comparable

    attr_accessor :id, :label, :barcode, :demographic_id

    def initialize
      @new = TRUE
    end

    def self.create_new(data)
      instance = self.new
      instance.demographic_id = data[:demogaphic_id]
      return instance
    end

    def self.create_from_db(data)
      self.new.populate_from_db(data)
    end

    def self.create_from_ca(data)
      self.new.populate_from_ca(data)
    end


    def self.find_by_id(id)
      sample = ::Sample.find_by_id(id)
      if sample.nil?
        NIL
      else
        self.new.populate_from_db(sample)
      end
    end

    def self.fields
      [:sample_class, :sample_type, :collection_time, :frozen_time, :haemolysed, :event_position, :location, :section_undertaken, :histology, :path_response_slides, :histology_text, :on_tma, :tma_cores, :core_location]
    end

    def new?
      @new ||= FALSE
    end

    def merge(an_other)
      clone = self.clone
      self.class.fields.each do |getter|
        clone.public_send(getter.to_s + "=",an_other.public_send(getter)) if clone.public_send(getter).nil?
      end
      return clone
    end

    def <=>(an_other)
      @label <=> an_other.label
    end

    def key
      Base64.encode64(label)[0..-4]
    end

    def method_missing(method, *args)
      the_method = method.to_s.gsub('=','')
      if self.class.fields.include? the_method.to_sym
        if (the_method==method.to_s)
          if self.instance_variable_defined?("@#{the_method}")
            return self.instance_variable_get("@#{the_method}") 
          else
            return NIL
          end
        else
          return self.instance_variable_set("@#{the_method}",args[0])
        end
      end
      super(method, *args)
    end

    def respond_to?(method)
      self.class.fields.include? method.to_s.gsub('=','').to_sym
    end

    def save
      commit
    end

    def destroy
      if not @new
        persisting_class.where(:label => @label,:barcode => @barcode).destroy_all
      else
        FALSE
      end
    end


    def parameters
      extra_fields_to_hash.merge({:label => @label, :barcode => @barcode, :id => @id, :demographic_id => @demographic_id}).reject { |k,v| v.nil? }
    end

    def update_attributes(attrs)
      symbolize_keys(attrs).each do |k,v|
        if self.class.fields.include? k
          self.public_send("#{k}=", v)
        end
      end
      save
    end

    def collection_time=(time)
      @collection_time = (time.is_a?(String) ? time.to_time : time)
    end

    def populate_from_ca(data)
      @new         = FALSE
      @label       = data[:label]
      @barcode     = data[:barcode]

      @collection_time = data[:collected_time]
      @frozen_time = data[:frozen_time]

      return self
    end

    def populate_from_db(data)
      @new            = FALSE
      @id             = data.id
      @label          = data.label
      @barcode        = data.barcode
      @demographic_id = data.demographic_id

      YAML::load(data.extra).each do |k,v|
        self.public_send("#{k}=",v)
      end

      return self
    end

    private

    def symbolize_keys(hash)
      hash.inject({}) { |hsh,v| hsh.merge({v.first.to_sym => v.last}) }
    end

    def extra_fields_to_hash
      self.class.fields.inject({}) do |hash,field|
        hash.merge({field => self.public_send("#{field}")})
      end
    end

    def persisting_class
      ::Sample
    end

    def commit
      ps = persisting_class.where(:label => @label).first_or_create!(:barcode => @barcode)
      extra = self.class.fields.inject(Hash.new) do |hsh,field| 
        hsh[field] = self.instance_variable_get("@#{field}") if self.instance_variable_defined?("@#{field}")
        hsh
      end
      ps.extra = extra.to_yaml

      if ps.save
        @new = FALSE
        return TRUE
      else
        return FALSE
      end
    end
  end
end
