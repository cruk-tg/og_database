module Proxies
  class SampleDecorator < Draper::Decorator
    delegate_all

    def sample_block
      readonly_fields.inject("") { |str,field| str += field_to_text(field) }
    end

    def field_to_text(field)
      h.content_tag(:b,field.to_s).concat(source.public_send("#{field}"))
    end

    def field_line(field)
      field_to_label(field).concat(field_to_input(field))
    end

    def field_to_label(field)
      h.content_tag(:label, field.to_s)
    end

    def field_to_input(field)
      h.text_field(:specimen, field)
    end

    def sample_class_list
      %w(Tissue Fluid Cell Molecular)
    end

    def readonly_fields
      [:label,:barcode,:sample_class,:sample_type]
    end

    def rw_fields
      source.class.fields - readonly_fields
    end

    def each_readwrite_field(&block)
      rw_fields.each do |field|
        case block.arity
        when 0
          yield field_line(field)
        when 1
          hsh = {:label => field_to_label(field), :input => field_to_input(field)}
          yield hsh
        when 2
          yield field_to_label(field), field_to_input(field)
        end
      end
    end
  end
end 
