module Proxies
  class Samples

    def self.find_by_participant(obj)
      self.new._find_by_participant(obj)
    end

    def _find_by_participant(obj)
      oe_number = obj.is_a?(String) ? obj : obj.participant_label
      remote_samples_from(RemoteData::CaDatastore,oe_number).each do |s|
        sample = Proxies::Sample.create_from_ca(s[:data])
        store(sample)
      end

      local_samples_from(::Sample,oe_number).each do |s|
        sample = Proxies::Sample.create_from_db(s)
        if (samples.contains_key?(sample.key))
          store(samples.get(sample.key).merge(sample))
        else
          store(sample)
        end
      end

      samples.all.each { |s| s.save }
      ::Sample.where("Label LIKE ?", oe_number + '%')
    end


    def local_samples_from(clazz,oe_number)
      clazz.where('label LIKE ?',oe_number + '%')
    end

    def remote_samples_from(clazz,oe_number)
      clazz.new.get oe_number
    end

    def oe_number
      @oe_number
    end

    def expiry_time
      5.minutes
    end

    def store(sample)
      samples.put(sample.key,sample,expiry_time.to_i)
    end

    def samples
      @samples 
    end

    protected

    def initialize
      @samples = TorqueBox::Infinispan::Cache.new(:name => 'samples')
      if @samples.class.include? TorqueBox::Injectors
        Rails.logger.info "Samples cache is valid"
      else
        Rails.logger.error "TorqueBox Injectors unavailable"
      end
    end
  end
end
