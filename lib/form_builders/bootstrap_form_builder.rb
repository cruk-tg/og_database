module FormBuilders
  class BootstrapFormBuilder < ActionView::Helpers::FormBuilder
    #delegate :hidden_field, :content_tag, :capture, :concat, to: :@template
    delegate :content_tag, :capture, :concat, to: :@template

    def input(attribute, options={})
      if not options.include? :html_options
        options[:html_options] = Hash.new
      end
      if not options.include? :as
        if ((@object.class.respond_to?(:reflect_on_association)) &&
            (@object.class.reflect_on_association (attribute)))
          options[:as] = :select
          options[:id_field] ||= :id
          options[:association] = @object.class.reflect_on_association attribute
        else
          options[:as] = :text
        end
      end
      self.public_send("#{options[:as]}_field",attribute,options)
    end

    def inputs(attribute, options={}, &block)
      content_tag(:div, class: 'well') do
      content_tag(:fieldset, options) do
        concat(content_tag(:legend) { field_name(attribute) })
        if block_given?
          contents = if @template.respond_to?(:is_haml?) && @template.is_haml?
                       @template.capture_haml(&block)
                     else
                       @template.capture(&block)
                     end
        end
        concat(content_tag(:div, :class => 'fieldset-content') { contents })
      end
      end
    end

    def actions(attributes=nil, options={})
      i18n_str = @object.new_record? ? 'helpers.submit.create' : 'helpers.submit.update'
      content_tag(:div, class: "input-group") do
        concat(submit(I18n.t(i18n_str, :model => @object.model_name.human),
                      data: "Please wait...",
                      class: "btn btn-primary btn-lg")
               )
      end
    end

    def semantic_fields_for(attribute, &block)

    end
    def has_many_select(attribute, options={})
      content_tag(:div, class: "input-group-lg") do
        concat(has_many_items(attribute, options))
      end
    end

    def has_many_items(attribute, options)
      content_tag(:ul, class: "list-group checked-list-box", :"data-database-attribute" => attribute) do
        options[:association].active_record.all.each do |record|
          concat(content_tag(:li, " #{record.name}", class: "list-group-item", :"data-database-id" => record.public_send(options[:id_field])))
        end
      end
    end

    def belongs_to_select(attribute, options={})
      within_input_group do
        concat(content_tag(:span, field_name(attribute).to_s, class: "input-group-addon", id:"#{attribute}-label-addon"))
        options[:value_method] = :id unless options.has_key?(:value_method)
        options[:text_method] = :name unless options.has_key?(:text_method)
        concat(collection_select(attribute, options))
      end
    end

    def select_field(attribute, options={})
      if !options.has_key?(:association) || options[:association].macro == :belongs_to
        belongs_to_select(attribute, options)
      elsif options[:association].macro == :has_many
        has_many_select(attribute, options)
      end
    end

    def text_field(attribute, options={})
      within_input_group do
        concat(content_tag(:span, field_name(attribute).to_s, class: "input-group-addon", id:"#{attribute}-label-addon"))
        concat(super(attribute, :class=> "form-control", :"aria-describedby" => "#{attribute}-label-addon"))
        Proc.new.call if block_given?
      end
    end

    def date_field(attribute, options={})
      text_field(attribute, options) do
        concat(calendar_group_addon)
      end
    end

    def text_area_field(attribute, options={})
      within_input_group do
        concat(content_tag(:span, field_name(attribute).to_s, class: "input-group-addon", id:"#{attribute}-label-addon")) unless options[:label] == :none
        concat(text_area(attribute, :class=> "form-control", :"aria-describedby" => "#{attribute}-label-addon"))
      end
    end

    def field_name(attribute)
      clazz = model_name.safe_constantize
      raise "#{model_name} does not exist" unless clazz
      clazz.human_attribute_name(attribute)
    end

    def collection_select(attribute,options={})
      if not options[:collection]
        options[:collection] = []
      end
      super(attribute, options[:collection], options[:value_method], options[:text_method],
            {prompt: true},
            {class: "form-control", :"aria-describedby" => "#{attribute}-label-addon"}.merge(options[:html_options]))
    end

    def fields_for(record_or_name_or_array, *args, &block)
      options = args.extract_options!
      options[:parent_builder] ||= self

      super(record_or_name_or_array, *(args << options), &block)
    end

    def check_boxes_field(attribute,options={})
      BootstrapCheckBoxBuilder.new(@template,@object).check_boxes_field(attribute,options)
    end

    private
    def within_input_group
      content_tag(:div, class: 'input-group', title: 'Loading...') do
        Proc.new.call if block_given?
      end
    end

    def model_name
      if @object.present?
        @object.class.name
      else
        if @object_name =~ /(.+)\[(.+)\]/
          $1.classify.safe_constantize.reflect_on_association($2).class_name
        else
          @object_name.to_s.classify
        end
      end
    end

    def calendar_group_addon
      content_tag(:span, class: "input-group-addon standard-addon") do
        content_tag(:span, '', :class => "fa fa-calendar")
      end
    end
  end
end
