module FormBuilders
  class BootstrapCheckBoxBuilder
    delegate :content_tag, :capture, :concat, to: :@template

    def initialize(template, object)
      @template = template
      @object   = object
    end

    def check_boxes_field(attribute,options={})
      @attribute = attribute
      content_tag(:div, class: 'check-box-group') do
        concat(heading)
        concat(hidden_checkbox)
        concat(button_group(options))
      end
    end

    private

    def attribute
      @attribute
    end

    def heading
      content_tag(:h3, field_name)
    end

    def hidden_checkbox
      @template.hidden_field_tag("#{model_name.underscore}[#{attribute.to_s.singularize}][]", nil,
                                 id: "#{model_name.underscore}_#{attribute}_none" )
    end

    def button_group(options)
      attr = attribute.to_s.singularize + '_ids'
      content_tag(:div, class: "btn-group") do
        options[:collection].each do |item|
          concat(build_bootstrap_btn_group_checks(item, attr))
        end
      end
    end

    def build_bootstrap_btn_group_checks(item, attr)
      content_tag(:label, class: 'btn btn-info') do
        concat(check_box(item.name,attr,item.id))
        concat(item.name)
      end
    end

    def check_box(name, attr, value)
      selected = ids.include?(value)
      @template.check_box_tag("#{model_name.underscore}[#{attr}][]", value, selected,
                              autocomplete: 'off',
                              id: "#{model_name.underscore}_#{attr}_#{value}")
    end

    def model_name
      if @object.present?
        @object.class.name
      else
        if @object_name =~ /(.+)\[(.+)\]/
          $1.classify.safe_constantize.reflect_on_association($2).class_name
        else
          @object_name.to_s.classify
        end
      end
    end

    def ids
      @ids ||= @object.public_send(attribute.to_s.singularize + "_ids")
    end

    def field_name
      clazz = model_name.safe_constantize
      raise "#{model_name} does not exist" unless clazz
      clazz.human_attribute_name(attribute)
    end

  end
end
