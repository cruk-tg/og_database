module Validators
  class ECMCIdValidator < ActiveModel::Validator
    def validate(record)
      options[:fields].each do |field|
        if record.public_send(field).nil? || record.public_send(field).match(/#{options[:project_id]}\d+/).nil?
          record.errors[field] << I18n.t(:invalid_ecmc_id)  
        end
      end
    end
  end
end
