module Validators
  class OccamsIdValidator < ActiveModel::Validator
    def validate(record)
      options[:fields].each do |field|
        if (record.public_send(field).nil? || (record.public_send(field).start_with? "OCCAMS/ED/" == false))
          record.errors[field] << I18n.t(:invalid_occams_id)
        end
      end
    end
  end
end
