module Validators
  class FollowUpValidator < ActiveModel::Validator

    def validate(record)
      if record.dod.is_a?(Date) && record.dls.is_a?(Date) && (record.dod < record.dls)
        record.errors.add('dod', 'Date Last Seen must be less than date of death')
      end
      if (!record.censor_value.nil?) && (record.censor_value == 'Alive') && (!record.dod.nil?)
        record.errors.add('dod', 'Marked as alive but with date of death')
      end
    end
  end
end
