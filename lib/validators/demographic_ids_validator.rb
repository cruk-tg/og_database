# Since Demographics as a number of IDs
# We use a custom validation class to
# valid the ID and the dates for those
# that have IDs
module Validators
  class DemographicIdsValidator < ActiveModel::Validator
    Fields = %w{occams_id oe_number chi}
    def validate(record)
      @record_under_validation = record
      Fields.each do |field|
        if !self.public_send("is_#{field}_valid?")
          record.errors[field] << I18n.t("invalid_#{field}")
        end
      end
    end

  def is_occams_id_valid?
    if (@record_under_validation.occams_id.nil?) || (@record_under_validation.occams_id.empty?) || (!@record_under_validation.occams_id.match(/OCCAMS\/ED\/\d{3}/).nil?)
      true
    else
      false
    end
  end

  def is_oe_number_valid?
    if ((@record_under_validation.oe_number.nil?)||(@record_under_validation.oe_number.match(/O[C|E]\d+/).nil?)||(@record_under_validation.oe_number.length != 8))
      false
    else
      true
    end
  end

  def is_chi_valid?
    (!@record_under_validation.chi.nil?) && (@record_under_validation.chi.length == 10) && !@record_under_validation.chi.match(/\d+/).nil?
  end

  end
end
