module Reporting
  class Definitions
    def initialize
      @file = File.open(Rails.root.join('config', 'definitions_for_report.tsv'))
      load
    end

    def pre_diagnosis
      @pre_diagnosis
    end

    def staging
      @staging
    end

    def treatment
      @treatment
    end

    def pathology
      @pathology
    end

    def follow_up
      @follow_up
    end

    private
    def load
      @pre_diagnosis = load_section
      @staging       = load_section
      @treatment     = load_section
      @pathology     = load_section
      @follow_up     = load_section
    end

    def load_section
      @buffer = []
      begin
        while(line = @file.readline)
          parts = line.split("\t")
          return @buffer if parts[0].strip.length == 0
          next if parts.length == 2 && parts[1].strip.length == 0
          parts[0] = parts[0].strip
          parts[1] = parts[1].strip
          @buffer << parts
        end
      rescue EOFError
        # Good
      end
      @buffer
    end
  end
end
