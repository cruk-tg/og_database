module Reporting
  module XlsData
    def field_data
      self.xls_fields.collect do |s|
        self.public_send(s)
      end
    end

    def xls_data
      xls_fields.collect do |field_name|
        if self.public_send(field_name).is_a? ActiveRecord::Associations::CollectionProxy
          self.public_send("#{field_name}_list")
        else
          self.public_send(field_name)
        end
      end
    end
  end
end
