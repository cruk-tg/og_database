module Reporting
  module XlsFields
    def ordered_fields
      []
    end

    def xls_fields
      array = ordered_fields + base_fields + lookup_fields
      if extra_fields.is_a?(Array)
        array += extra_fields
      else
        array += extra_fields.keys
      end
      array.uniq
    end

    def base_fields
      attribute_rejector = -> (h) { h.ends_with?("id") || h.ends_with?("_at") || global_ignore.include?(h) }
      self.attribute_names.reject(&attribute_rejector).collect(&:to_sym)
    end

    def lookup_fields
      []
    end

    def global_ignore
      %w(demographic exposure staging treatment pathology follow_up ffpe samples)
    end

    def local_ignore
      []
    end

    def name_value(field)
      if self.public_send(field).respond_to? :name
        XmlType.new :string, self.public_send(field).name
      else
        XmlType.new :string, ""
      end
    end
  end
end
