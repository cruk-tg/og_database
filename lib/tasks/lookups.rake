namespace :og_database do
  task :clean => :environment do
    Rake::Task['db:drop'].execute
    Rake::Task['db:create'].execute
    Rake::Task['db:schema:load'].execute
    Rake::Task['og_database:populate']
  end

  task :populate => :environment do |t|
    Rake::Task["og_database:tnms"].execute
    Rake::Task["og_database:previous_barretts_managements"].execute
    Rake::Task["og_database:co_morbidity_lookups"].execute
    Rake::Task["og_database:smoker_lookups"].execute
    Rake::Task["og_database:relationships"].execute
    Rake::Task["og_database:exercise_tolerances"].execute
    Rake::Task["og_database:asa_grades"].execute
    Rake::Task["og_database:presenting_complaint_lookups"].execute
    Rake::Task["og_database:referral_sources"].execute
    Rake::Task["og_database:drugs"].execute
    Rake::Task["og_database:length_of_use"].execute
    Rake::Task["og_database:histologies"].execute
    Rake::Task["og_database:tumour_locations"].execute
    Rake::Task["og_database:post_chemotherapy_restaging_lookups"].execute
    Rake::Task["og_database:yes_no_unknown"].execute
    Rake::Task["og_database:intent_lookups"].execute
    Rake::Task["og_database:treatment_intent_lookups"].execute
    Rake::Task["og_database:radiotherapy_intent_lookups"].execute
    Rake::Task["og_database:chemotherapy_regimens"].execute
    Rake::Task["og_database:surgery_consultants"].execute
    Rake::Task["og_database:surgery_operations"].execute
    Rake::Task["og_database:gradings"].execute
    Rake::Task["og_database:barretts_adjacent_lookups"].execute
    Rake::Task["og_database:resection_lookups"].execute
    Rake::Task["og_database:censor_value"].execute
    Rake::Task["og_database:evidence_for_recurrences_lookups"].execute
    Rake::Task["og_database:histological_response"].execute
    Rake::Task["og_database:genders"].execute
    Rake::Task["og_database:washings_reports"].execute
    Rake::Task["og_database:primary_tumour_extension"].execute
    Rake::Task["og_database:chemotherapy_why_off_lookups"].execute
    Rake::Task["og_database:responses"].execute
    Rake::Task["og_database:local_recurrences"].execute
    Rake::Task["og_database:metastasis_pathologies"].execute
  end

  task :tnms => :environment do
    ts = %w{Tx T1a T1b T2 T3 T4a T4b NA}
    ns = %w{N0 N1 N2 N3 N3a N3b NA}
    ms = %w{M0 M1 NA}
    ts.each do |t|
      TnmT.create!(:name => t)
    end
    ns.each do |n|
      TnmN.create!(:name => n)
    end
    ms.each do |m|
      TnmM.create!(:name => m)
    end
  end

  task :previous_barretts_managements => :environment do
    pbms = %w{Nil Surveillance EMR Radiofrequency Photodynamic Other}
    pbms.each do |pbm|
      PreviousBarrettManagement.create :name => pbm
    end
  end

  task :co_morbidity_lookups => :environment do
    coms = ["Nil", "Cardiovascalar", "Renal impairment", "Liver impairment", "Thromboembolic", "Diabetes"]
    coms.each do |com|
      CoMorbidityLookup.create :name => com
    end
  end

  task :smoker_lookups => :environment do
    smoker_lookups = %w{Current Ex-Smoker Never}
    smoker_lookups.each do |smoker_lookup|
      Smoker.create :name => smoker_lookup
    end
  end

  task :relationships => :environment do
    relationships = %w{Grand-Daughter Daughter Mother Grand-Mother Aunt Grand-Son Son Father Grand-Father Uncle Sister Brother}
    relationships.each do |relationship|
      Relationship.create :name => relationship
    end
  end

  task :exercise_tolerances => :environment do
    exercose_tolerances = %w{0-100m 100-1000m unlimited}
    exercose_tolerances.each do |exercise_tolerance|
      ExerciseTolerance.create :name => exercise_tolerance
    end
  end

  task :asa_grades => :environment do
    asa_grades = %w{I II III IV V}
    asa_grades.each do |asa_grade|
      AsaGrade.create :name => asa_grade
    end
  end

  task :presenting_complaint_lookups => :environment do
    presenting_complaints = ["Incidental diagnosis", "Dysphagia", "Bleeding/Anaemia", "Weight Loss", "Reflux", "Other"]
    presenting_complaints.each do |presenting_complaint|
      PresentingComplaintLookup.create :name => presenting_complaint
    end
  end

  task :referral_sources => :environment do
    referral_sources = %w{GP Hospital}
    referral_sources.each do |referral_source|
      ReferralSource.create :name => referral_source
    end
  end

  task :drugs => :environment do
    drugs = ["Aspirin", "PPI", "Non-aspirin Anticoagulant", "Statin"]
    drugs.each do |drug|
      Drug.create :name => drug
    end
  end

  task :length_of_use => :environment do
    length_of_uses = ["<6 months", "6-24 months", "> 24 months"]
    length_of_uses.each do |length_of_use|
      LengthOfUse.create :name => length_of_use
    end
  end

  task :histologies => :environment do
    histologies = ["Adenocarcinoma", "Squamous cell carcinoma", "Adenosquamous", "Undifferentiated", "Small cell", "Other epithelial", "Other non-epithelial", "Benign"]
    histologies.each do |histology|
      Histology.create :name => histology
    end
  end

  task :tumour_locations => :environment do
    tumour_locations = ["Oesophagus upper 1/3", "Oesophagus mid 1/3", "Oesophagus lower 1/3", "Gastro-oesophageal junction Siewert Type I", "Gastro-oesophageal junction Siewert Type II", "Gastro-oesophageal junction Siewert Type III", "Stomach Fundus", "Stomach Body", "Stomach Antrum", "Stomach Pylorus"]
    tumour_locations.each do |tumour_location|
      TumourLocation.create :name => tumour_location
    end
  end

  task :post_chemotherapy_restaging_lookups => :environment do
    post_chemotherapy_restaging_lookups = %w{CT CT-PET}
    post_chemotherapy_restaging_lookups.each do |post_chemotherapy_restaging_lookup|
      PostChemotherapyRestagingLookup.create :name => post_chemotherapy_restaging_lookup
    end
  end

  task :yes_no_unknown => :environment do
    yes_no_unknowns = %w{Yes No Unknown}
    yes_no_unknowns.each do |yes_no_unknown|
      YesNoUnknown.create :name => yes_no_unknown
    end
  end

  task :intent_lookups => :environment do
    intent_lookups = %w{Neoadjuvant Curative Palliative None}
    intent_lookups.each do |intent_lookup|
      IntentLookup.create :name => intent_lookup
    end
  end

  task :treatment_intent_lookups => :environment do
    intent_lookups = %w{Curative Palliative None}
    intent_lookups.each do |intent_lookup|
      TreatmentIntentLookup.create :name => intent_lookup
    end
  end

  task :radiotherapy_intent_lookups => :environment do
    intent_lookups = %w{Neoadjuvant Adjuvant Curative Palliative None}
    intent_lookups.each do |intent_lookup|
      RadiotherapyIntentLookup.create :name => intent_lookup
    end
  end

  task :chemotherapy_regimens => :environment do
    chemotherapy_regimens = %w{2xCisplatin/5-FU 3xEpirubicin/Cisplatin/Capecitabine(ECX) 3xEpirubicin/Oxaliplatin/Capecitabine(EOX) 6xEpirubicin/Cisplatin/5-FU(ECF)}
    chemotherapy_regimens.each do |chemotherapy_regimen|
      ChemotherapyRegimen.create :name => chemotherapy_regimen
    end
  end

  task :surgery_consultants => :environment do
    surgery_consultants = %w{SPB AP CD GWC PL}
    surgery_consultants.each do |surgery_consultant|
      SurgeryConsultant.create :name => surgery_consultant
    end
  end

  task :surgery_operations => :environment do
    general = ["Ivor-Lewis Oesophagectomy", "Thoracopsoic-Assisted Oesophagectimy", "McKeown Three-Phase Oesophagectomy", "Left Thoracoabdominal Gastrectomy", "Extended Total Gastrectomy", "Transhiatial Oesophagectomy", "Laparoscopy only", "Laparomtomy Only", "Other"]
    abdominal_phase = %w{Open Laparoscopic Lap-to-Open}
    thoracic_phase = %w{Open Laparoscopic Lap-to-Open NA}
    general.each do |g|
      SurgeryOperation.create :name => g, :phase => 'general'
    end

    abdominal_phase.each do |a|
      SurgeryOperation.create :name => a, :phase => 'abdominal'
    end

    thoracic_phase.each do |t|
      SurgeryOperation.create :name => t, :phase => 'thoracic'
    end
  end

  task :gradings => :environment do
    gradings = %w{Well Moderate Poor Undifferentiated Unknown}
    gradings.each do |grading|
      Grading.create :name => grading
    end
  end

  task :barretts_adjacent_lookups => :environment do
    barretts_adjacent_lookups = ["Macroscopic", "Microscopic gastric metaplasia", "Microscopic intestinal metaplasia", "Microscopic low grade dysplasia", "Microscopic high grade dysplasia", "None"]
    barretts_adjacent_lookups.each do |barretts_adjacent_lookup|
      BarrettsAdjacentLookup.create :name => barretts_adjacent_lookup
    end
  end

  task :resection_lookups => :environment do
    resection_lookups = %w{R0 R1 R2}
    resection_lookups.each do |resection_lookup|
      ResectionLookup.create :name => resection_lookup
    end
  end

  task :censor_value => :environment do
    censor_values = %w{Alive Dead Unknown}
    censor_values.each do |censor_value|
      CensorValue.create :name => censor_value
    end
  end

  task :evidence_for_recurrences_lookups => :environment do
    evidence_for_recurrences = ["Clinical", "CT", "Other Radiology", "Pathological"]
    evidence_for_recurrences.each do |evidence_for_recurrence|
      EvidenceForRecurrenceLookup.create :name => evidence_for_recurrence
    end
  end

  task :histological_response => :environment do
    histological_responses = ["TRG I", "TRG II", "TRG III", "TRG IV", "TRG V"]
    histological_responses.each do |histological_response|
      HistologicalResponse.create :name => histological_response
    end
  end

  task :genders => :environment do
    genders = %w{Male Female}
    genders.each do |gender|
      Gender.create :name => gender
    end
  end

  task :washings_reports => :environment do
    washings_reports = ["No evidence of malignancy", "Suspicious for Malignancy", "Malignant cell", "Non-diagnostic"]
    washings_reports.each do |washings_report|
      WashingsReport.create :name => washings_report
    end
  end

  task :primary_tumour_extension => :environment do
    primary_tumour_extensions = %w{None Serosa Crus}
    primary_tumour_extensions.each do |primary_tumour_extension|
      PrimaryTumourExtension.create :name => primary_tumour_extension
    end
  end

  task :chemotherapy_why_off_lookups => :environment do
    chemotherapy_why_off_lookups = ["As specified", "Incomplete due to complications", "Incomplete due to patient choice", "Not started"]
    chemotherapy_why_off_lookups.each do |chemotherapy_why_off_lookup|
      ChemotherapyWhyOffLookup.create :name => chemotherapy_why_off_lookup
    end
  end

  task :responses => :environment do
    responses = ["complete response", "partial response", "stable disease", "progression now palliative", "progression still curative"]
    responses.each do |response|
      Response.create :name => response
    end
  end

  task :local_recurrences => :environment do
    local_recurrences = ["Local", "Distant", "Local and Distant"]
    local_recurrences.each do |local_recurrence|
      LocalRecurrence.create :name => local_recurrence
    end
  end

  task :metastasis_pathologies => :environment do
    metastasis_pathologies = ["Metastasis", "Primary", "Non-diagnostic", "Benign"]
    metastasis_pathologies.each do |metastasis_pathology|
      MetastasisPathology.create :name => metastasis_pathology
    end
  end
end
