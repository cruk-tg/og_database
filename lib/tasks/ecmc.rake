namespace :ecmc do
  def collect_values(table, fields)
    table.all.collect do |record|
      hsh = Hash.new { |hash,key| raise "#{key} is not present in hash"}
      hsh.merge(record.attributes.keep_if { |attribute| attribute.in? fields })
    end
  end

  def update_table(clazz, records)
    if not clazz.connection.config[:database].include?("production")
      clazz.send(:establish_connection, "production")
      clazz.connection.schema_cache.clear!
      clazz.reset_column_information
    end
    records.each do |record|
      clazz.update record.delete(:id), record
    end
  end

  desc "Merges data together"
  task :merge => :environment do
    Rake::Task["ecmc:merge_laparoscopy"].execute
    Rake::Task["ecmc:merge_pathology"].execute
    Rake::Task["ecmc:merge_exposure"].execute
    Rake::Task["ecmc:merge_stagings_tnm"].execute
    Rake::Task["ecmc:merge_treatments"].execute
    Rake::Task["ecmc:merge_follow_ups"].execute
    Rake::Task["ecmc:merge_post_chemotherapy_restagings"].execute
    Rake::Task["ecmc:merge_stagings"].execute
  end

  task :merge_laparoscopy => :environment do
    fields = %w(id tumour_location_id dmpd_id ascites_id mom_id primary_tumour_extension_id washings_report_id metastasis_pathology_id)

    values = collect_values Laparoscopy, fields
    updates = values.collect do |value|
      {
        id: value["id"],
        tumour_location: value["tumour_location_id"].nil? ? nil : TumourLocation.find(value["tumour_location_id"]).name,
        dmpd: value["dmpd_id"].nil? ? nil : YesNoUnknown.find(value["dmpd_id"]).name,
        ascites: value["ascites_id"].nil? ? nil : YesNoUnknown.find(value["ascites_id"]).name,
        mom: value["mom_id"].nil? ? nil : YesNoUnknown.find(value["mom_id"]).name,
        primary_tumour_extension: value["primary_tumour_extension_id"].nil? ? nil : PrimaryTumourExtension.find(value["primary_tumour_extension_id"]).name,
        washings_report: value["washings_report_id"].nil? ? nil : WashingsReport.find(value["washings_report_id"]).name,
        metastasis_pathology: value["metastasis_pathology_id"].nil? ? nil : MetastasisPathology.find(value["metastasis_pathology_id"]).name
      }
    end
    update_table Laparoscopy, updates
  end

  task :merge_pathology => :environment do
    fields = %w{id tumour_location_id histology_id grade_id resection_id lymphovascalar_invasion_id perineural_invasion_id venous_invasion_id tnm_t_id tnm_n_id tnm_m_id nodes_on_both_sides_id histological_response_id}

    values = collect_values Pathology, fields

    updates = values.collect do |value|
      {
        id: value["id"],
        tumour_location: value["tumour_location_id"].nil? ? nil : TumourLocation.find(value["tumour_location_id"]).name,
        histology: value["histology_id"].nil? ? nil : Histology.find(value["histology_id"]).name,
        grade: value["grade_id"].nil? ? nil : Grading.find(value["grade_id"]).name,
        resection: value["resection_id"].empty? ? nil : ResectionLookup.find(value["resection_id"]).name,
        lymphovascalar_invasion: value["lymphovascalar_invasion_id"].nil? ? nil : YesNoUnknown.find(value["lymphovascalar_invasion_id"]).name,
        perineural_invasion: value["perineural_invasion_id"].nil? ? nil : YesNoUnknown.find(value["perineural_invasion_id"]).name,
        venous_invasion: value["venous_invasion_id"].nil? ? nil : YesNoUnknown.find(value["venous_invasion_id"]).name,
        tnm_t: value["tnm_t_id"].nil? ? nil : TnmT.find(value["tnm_t_id"]).name,
        tnm_n: value["tnm_n_id"].nil? ? nil : TnmN.find(value["tnm_n_id"]).name,
        tnm_m: value["tnm_m_id"].nil? ? nil : TnmM.find(value["tnm_m_id"]).name,
        nodes_on_both_sides: value["nodes_on_both_sides_id"].nil? ? nil : YesNoUnknown.find(value["nodes_on_both_sides_id"]).name,
        histological_response: value["histological_response_id"].nil? ? nil : HistologicalResponse.find(value["histological_response_id"]).name
      }
    end

    update_table Pathology, updates
  end

  task :merge_exposure => :environment do
    fields = %w(id previous_alcohol_excess_id exercise_tolerance_id asa_grade_id referral_source_id)
    values = collect_values Exposure, fields

    updates = values.collect do |value|
      {
        id: value["id"],
        previous_alcohol_excess: value["previous_alcohol_excess_id"].nil? ? nil : YesNoUnknown.find(value["previous_alcohol_excess_id"]).name,
        exercise_tolerance: value["exercise_tolerance_id"].nil? ? nil : ExerciseTolerance.find(value["exercise_tolerance_id"]).name,
        asa_grade: value["asa_grade_id"].nil? ? nil : AsaGrade.find(value["asa_grade_id"]).name,
        referral_source: value["referral_source_id"].nil? ? nil : ReferralSource.find(value["referral_source_id"]).name
      }
    end

    update_table Exposure, updates
  end

  task :merge_stagings_tnm => :environment do
    belongs_to = {:tnm_t => TnmT, :tnm_n => TnmN, :tnm_m => TnmM}
    tablenames  = [StagingCt, StagingCtpet, FinalMdtStage, StagingEndoscopicUltrasound]

    tablenames.each do |tablename|
      fields = belongs_to.keys.collect { |key| "#{key}_id" }
      fields << "id"
      values = collect_values tablename, fields

      updates = values.collect do |value|
        {
        id: value["id"],
        tnm_t: value["tnm_t_id"].nil? ? nil : TnmT.find(value["tnm_t_id"]).name,
        tnm_n: value["tnm_n_id"].nil? ? nil : TnmN.find(value["tnm_n_id"]).name,
        tnm_m: value["tnm_m_id"].nil? ? nil : TnmM.find(value["tnm_m_id"]).name
        }
      end

      update_table tablename, updates
    end
  end

  task :merge_treatments => :environment do
    Treatment.send(:establish_connection, "development")
    fields = %w(id preop_chemo_reg_id postop_chemo_reg_id intent_id preop_chemo_type_id preop_radio_type_id patient_received_deuterium_id surgery_happened_id surgery_consultant_id surgery_operation_id abdominal_phase_id thoracic_phase_id preop_chemo_whyoff_id)
    values = collect_values Treatment, fields

    updates = values.collect do |value|
      {
        id: value["id"],
        chemotherapy_why_off_lookup: value["preop_chemo_whyoff_id"].nil? ? nil : ChemotherapyWhyOffLookup.find(value["preop_chemo_whyoff_id"]).name,
        treatment_intent: value["intent_id"].nil? ? nil : TreatmentIntentLookup.find(value["intent_id"]).name,
        preop_chemo_reg: value["preop_chemo_reg_id"].empty? ? nil : ChemotherapyRegimen.find(value["preop_chemo_reg_id"]).name,
        postop_chemo_reg: value["postop_chemo_reg_id"].nil? ? nil : ChemotherapyRegimen.find(value["postop_chemo_reg_id"]).name,
        preop_chemo_type: value["preop_chemo_type_id"].nil? ? nil : IntentLookup.find(value["preop_chemo_type_id"]).name,
        radio_type: value["preop_radio_type_id"].nil? ? nil : RadiotherapyIntentLookup.find(value["preop_radio_type_id"]).name,
        patient_received_deuterium: value["patient_received_deuterium_id"].nil? ? nil : YesNoUnknown.find(value["patient_received_deuterium_id"]).name,
        surgery_happened: value["surgery_happened_id"].nil? ? nil : YesNoUnknown.find(value["surgery_happened_id"]).name,
        surgery_consultant: value["surgery_consultant_id"].nil? ? nil : SurgeryConsultant.find(value["surgery_consultant_id"]).name,
        surgery_operation: value["surgery_operation_id"].nil? ? nil : SurgeryOperation.find(value["surgery_operation_id"]).name,
        abdominal_phase: value["abdominal_phase_id"].nil? ? nil : SurgeryOperation.find(value["abdominal_phase_id"]).name,
        thoracic_phase: value["thoracic_phase_id"].nil? ? nil : SurgeryOperation.find(value["thoracic_phase_id"]).name,

      }
    end
    update_table Treatment, updates
  end

  task :merge_follow_ups => :environment do
    FollowUp.send(:establish_connection, "development")
    fields = %w(id censor_value_id local_recurrence_id)
    values = collect_values FollowUp, fields

    updates = values.collect do |value|
      {
        id: value["id"],
        censor_value: value["censor_value_id"].nil? ? nil : CensorValue.find(value["censor_value_id"]).name,
        local_recurrence: value["local_recurrence_id"].nil? ? nil : LocalRecurrence.find(value["local_recurrence_id"]).name
      }
    end

    update_table FollowUp, updates
  end

  task :merge_post_chemotherapy_restagings => :environment do
    PostChemotherapyRestaging.send(:establish_connection, "development")
    fields = %w(id pcr_type_id finding_id)
    values = collect_values PostChemotherapyRestaging, fields

    updates = values.collect do |value|
      {
        id: value["id"],
        pcr_type: value["pcr_type_id"].nil? ? nil : PostChemotherapyRestagingLookup.find(value["pcr_type_id"]).name,
        finding: value["finding_id"].nil? ? nil : Response.find(value["finding_id"]).name
      }
    end

    update_table PostChemotherapyRestaging, updates
  end

  task :merge_stagings => :environment do
    Staging.send(:establish_connection, "development")
    fields = %w(id tumour_location_id histology_id)
    values = collect_values Staging, fields

    updates = values.collect do |value|
      {
        id: value["id"],
        tumour_location: value["tumour_location_id"].nil? ? nil : TumourLocation.find(value["tumour_location_id"]).name,
        histology: value["histology_id"].nil? ? nil : Histology.find(value["histology_id"]).name
      }
    end

    update_table Staging, updates
  end

  task :sync_samples => :environment do
    Demographic.where("oe_number NOT LIKE 'OE99%'").each do |demographic|
      CatissueViewSpecimen.where('label LIKE ?', "#{demographic.oe_number}%").each do |ca_specimen|
        puts demographic.oe_number
        sp = ca_specimen.to_hash
        if sp.is_a?(Hash) && sp.has_key?(:label) && (!sp[:label].nil?)
          s = Sample.where(:label => sp[:label]).first_or_initialize
          s.barcode = ca_specimen[:barcode]
          s.sample_type = ca_specimen[:specimen_type]
          s.sample_class = ca_specimen[:specimen_class]
          s.extra = s.extra.update(collection_status: ca_specimen[:collection_status])
          s.frozen_time = ca_specimen[:frozen_time]
          s.collection_time = ca_specimen[:collected_time]
          s.extra = s.extra.update(storage_location: ca_specimen[:storage_location])
          s.demographic = demographic
          s.save!
        end
      end
    end
  end
  task :create => :environment do
    Rake::Task['db:create'].invoke
    Rake::Task['db:schema:load'].invoke
  end
  task :build do
    Rake::Task['assets:clobber'].invoke
    Rake::Task['log:clear'].invoke
    Rake::Task['tmp:clear'].invoke
    Rake::Task['tmp:create'].invoke
    app = Rails.app_class
    name = app.name.underscore.split('/').first
    version = app.config.x.version
    system "docker build -t vcs.ecmc.ed.ac.uk/ecmc/#{name}:#{version} ."
    system "git tag --force '#{version}'"
    system "git push --tags"
  end
  task :start => :environment do
    if ActiveRecord::Migrator.migrations_status(Rails.root).select { |a| a[0] != "up" }.any?
      conn = ActiveRecord::Base.connection
      conn.execute "SET GLOBAL pxc_strict_mode=MASTER;"
      conn.execute "SET GLOBAL wsrep_OSU_method='TOI';"
      Rake::Task['db:migrate'].invoke
      conn.execute "SET GLOBAL pxc_strict_mode=ENFORCING;"
    end
    Rake::Task['assets:precompile'].invoke
    exec 'bundle exec rails server -b 0.0.0.0'
  end

  task :fix_pathology => :environment do
    yes_no_fields = [:signet_ring_cells, :nodes_on_both_sides,
                     :lymphovascalar_invasion, :perineural_invasion,:venous_invasion]
    classified = [:tumour_location, :histology, :histological_response]

    Pathology.all.each do |p|
      is_int = -> (f) { p.public_send("#{f}?") && (p.public_send(f).to_i.to_s == p.public_send(f)) }
      field_update = -> (f) do
        p.public_send("#{f}=", YesNoUnknown.find(p.public_send(f)).name)
      end
      classified_update = -> (f) do
        c = f.to_s.classify.safe_constantize
        p.public_send("#{f}=", c.find(p.public_send(f)).name)
      end
      yes_no_fields.select(&is_int).each(&field_update)
      classified.select(&is_int).each(&classified_update)
      if (is_int.call(:grade))
        p.grade = Grading.find(p.grade).name
      end
      if (is_int.call(:resection))
        p.resection = ResectionLookup.find(p.resection).name
      end
      p.save
    end
  end

  task :fix_follow_ups => :environment do
    classified = [:censor_value, :local_recurrence]
    FollowUp.all.each do |p|
      is_int = -> (f) { p.public_send("#{f}?") && (p.public_send(f).to_i.to_s == p.public_send(f)) }
      classified_update = -> (f) do
        c = f.to_s.classify.safe_constantize
        p.public_send("#{f}=", c.find(p.public_send(f)).name)
      end
      classified.select(&is_int).each(&classified_update)
      p.save
    end
  end

  task :fu_import => :environment do
    inp = $stdin.each_line do |line|
      csv = line.split(',')
      demographic = Demographic.find_by_oe_number(csv[0])
      if demographic.nil?
        puts "Unable to find #{csv[0]}"
        next
      end
      follow_up = FollowUp.where(:demographic_id => demographic.id).first_or_create!

      follow_up.censor_value = csv[1]
      follow_up.dod          = csv[2].to_date
      follow_up.dls          = csv[3].to_date

      if !follow_up.save
        puts "Error on #{csv[0]}"
        puts follow_up.errors.full_messages.join(" ")
      else
        puts "Updated: #{csv[0]}"
      end
    end
  end
end
