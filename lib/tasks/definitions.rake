namespace :og_database do
  task :definitions => :environment do
    demographic = {
      hospital_number: 'Hospital Number',
      chi: 'CHI',
      dob: 'Date of birth',
      gender_id: 'Gender',
      gp_name: 'GP Name',
      gp_address1: 'GP Address',
      gp_address2: 'GP Address',
      gp_address3: 'GP Address',
      gp_address4: 'GP Address',
      gp_phone_number: 'GP Phone number',
      date_of_ecmc_consent: 'Date of ECMC consent',
      date_of_occams_consent: 'Date of OCCAMS consent',
      occams_id: 'OCCAMS ID',
      oe_number: 'ECMC Role number',
      neo_aegis: 'Neo AEGIS'
    }

    exposure = {
      known_barretts: 'Y/N',
      barretts_length: 'Previously Known Barrett''s - endoscopic length(cm) at last endoscopy',
      prague_classification_c: 'Barrett''s Circumferential length(cm)',
      prague_classification_m: 'Barrett''s Maximum length (cm)',
      previous_alcohol_excess: 'Y/N',
      alcohol: 'Units per week',
      pulmonary_function_fev1_lm: 'Pre-operative PFTS - FEV1 in litres per minute',
      pulmonary_function_fev1_p: 'Pre-operative PFTS - FEV1 in % predicted',
      pulmonary_function_fvc_lm: 'Pre-operative PFTS - FVC in litres per minute',
      pulmonary_function_fvc_p:	'Pre-operative PFTS - FVC in % predicted',
      pulmonary_function_fev1_fvc_p: 'Pre-operative PFTS - FEV1/FVC in % predicted',
      egfr: 'estimated Glomerular filtration rate.',
      albumin: 'Pre-operative Albumin (last one before any OG cancer treatment) g/L',
      haemoglobin: 'Pre-operative Hb (last one before any OG cancer treatment) g/L',
      weight_loss: 'Total weight loss (kg)',
      weight_loss_weeks: 'Time weight lost over (weeks)',
      height: 'in metres',
      weight: 'in Kg at time of consent (usually pre-operative UGI clinic)',
      bmi: 'in Kg/metres squared',
      smoker: 'Current/Ex-smoker/Never',
      smoker_years: 'Number of years smoking',
      smoker_packs: 'packs per day',
      smoker_pack_years: 'pack years',
      exercise_tolerance: 'Distance can walk on flat ground (metres) choice of unlimited, 100-1000m, 0-100m',
      asa_grade: 'choice of: I - V',
      referral_source: 'GP/Hospital',
      presenting_complaints: 'Choice of Bleeding/Anaemia,  Dysphagia, Weight-loss, Incidental Diagnosis or Other',
      co_morbidities: 'Choice of Cardiovascular, Diabetes, Renal Impairment (eGFR < 60), Liver Impairment, Thromboembolic, Nil',
      drugs_list: 'Choice of Aspirin, PPI, Non-aspirin anticoagulant, statin',
      family_history_of_og_cancer: 'Relative(s) affected',
      family_history_of_other_cancer: 'Relative(s) affected'
    }

    staging = {
      date_of_diagnosis:	'Date of pathology report of diagnostic biopsy'	,
      tumour_location:	'From diagnostic endoscopy - AEG I-III, Oes lower, mid , upper 1/3'	,
      histology:	'of diagnostic biopsy'	,
      staging_ct_report:	'Free text from TRAK (or notes)'	,
      staging_ct_positive_nodes:	'Number of positive nodes on staging ct'	,
      staging_ct_tnm_t:	'T stage from CT report'	,
      staging_ct_tnm_n:	'N stage from CT report (use the data on number of positive nodes  - the summary N stage is occasionally wrong)'	,
      staging_ct_tnm_m:	'M stage from CT report - anything which prompts a further investigation is M1.'	,
      staging_ctpet_report:	'Free text (from TRAK)'	,
      staging_ctpet_tnm_t:	'T stage from CTPET report'	,
      staging_ctpet_tnm_n:	'N stage from CTPET report (use the data on number of positive nodes  - the summary N stage is occasionally wrong)'	,
      staging_ctpet_tnm_m:	'M stage from CTPET report - anything which prompts a further investigation is M1.'	,
      staging_endoscopic_ultrasound_report:	'Free text - from unisoft (GI reporting tool)'	,
      staging_endoscopic_ultrasound_tumour_from:	'Start of the tumour (cm from the incisors)'	,
      staging_endoscopic_ultrasound_tumour_to:	'End of the tumour (cm from the incisors)'	,
      staging_endoscopic_ultrasound_nodes:	'Number of positive nodes on staging EUS'	,
      staging_endoscopic_ultrasound_tnm_t:	'T stage from EUS'	,
      staging_endoscopic_ultrasound_tnm_n:	'N stage from EUS (use the data on number of positive nodes  - the summary N stage is occasionally wrong)'	,
      staging_endoscopic_ultrasound_tnm_m:	'M stage from EUS report - anything which prompts a further investigation is M1.'	,
      other_investigation_report:	'Other staging Investigations e.g. EBUS, neck USS etc.'	,
      other_investigation_metastatsis:	'Location of metastasis from other investigations'	,
      final_mdt_stage_tnm_t:	'Maximum T stage (accept EUS first then CT)'	,
      final_mdt_stage_tnm_n:	'Maximum N stage (accept EUS first then CT)'	,
      final_mdt_stage_tnm_m:	'Maximum M stage (from CT or CT-PET)'	,
      final_mdt_stage_mets_location:	'If M1 at any stage - where is the metastasis'	,
      post_chemotherapy_restaging_pcr_type:	'Did they have a CT or CT-PET for restaging post-chemo (all CT for now)'	,
      post_chemotherapy_restaging_finding:	'Complete response, partial response, stable disease, progression still curative, progression now palliative'	,
    }

    laparoscopy = {
      macroscopic_findings:	'From laparoscopy report - appears on LSA and often trak. Pink op note sheets in notes.'	,
      tumour_location:	'Based on endoscopy at the time of laparoscopy - same options as "tumour location"'	,
      dmpd:	'Distant macroscopic peritoneal disease?'	,
      ascites:	'Is there ascites ?'	,
      mom:	'Is there macroscopic organ metastasis'	,
      primary_tumour_extension:	'Does the primary tumour extend to the serosa, the diaphramatic crus or neither'	,
      washings_report:	'No malignancy, suspcious, malignant cells, non-diagnostic'	,
      metastasis_pathology:	'Biopsy of macroscopic suspected metastasis - Primary tumour, metastasis, benign, non-diagnostic'	,
    }

    treatment = {
      preop_chemo_type:	'Neoadjuvant, palliative, attempted curative or none'	,
      preop_chemo_reg:	'OE02 (2xCisplatin 5-Fluorouracil 2xCF), OE05 (4xEpirubicin, Cisplatin and Capecitabine (ECX)), MAGIC (3xECX pre and post op), Pallitiave (Epirubinc Oxaliplatin Capecitabine EOX, Epirubinc Csiplatin 5-FU)'	,
      preop_chemo_complications:	'Free text'	,
      chemotherapy_why_off:	'Chemo completed? As specified, incomplete due to complications, incomplete due to patient choice'	,
      surgery_happened:	'Y/N'	,
      surgery_consultant:	'Supervising consultant in theatre'	,
      surgery_operation:	'Ivor-lewis, Left thoracoabdominal, Thoracoscopic-assisted oesophagectomy, Extended total gastrectomy, Transhiatal or non-resectional e.g. laparotomy only, thoractomy only, laparoscopy only'	,
      abdominal_phase:	'Open/Lap/ Lap  - open'	,
      thoracic_phase:	'Open/Lap/ Lap  - open'	,
      radio_type:	'Radiotherapy at any time - palliative, adjuvant'	,
      radio_dose:	'in Gy (usually 50Gy for Adjuvant)'	,
      radio_fractions:	'20 for Adjuvant'	,
      radio_complications:	'as chemo'	,
      postop_chemo_reg:	'e.g. palliative EOX or post-op ECX'	,
      patient_received_deuterium:	'Part of KCHF study?'	,
    }

    pathology = {
      diagnostic_pathology_no:	'reference no. for diagnostic pathology blocks'	,
      pathology_number:	'reference no. for resection pathology blocks'	,
      maximum_tumour_diameter:	'Largest dimension (any) in millimetres'	,
      distance_to_proximal_margin:	'in mm'	,
      distance_to_distal_margin:	'in mm'	,
      distance_to_circumferential_resection_margin:	'in mm'	,
      number_of_nodes:	'total number of nodes resected'	,
      postive_nodes:	'total number of positive nodes identified'	,
      location_of_involved_nodes:	'location of positive nodes'	,
      nodes_on_both_sides:	'are the positive nodes on both sides of the diaphragm'	,
      tumour_location:	'e.g. GOJ type I , II, II or oes upper middle or lower 1/3'	,
      histology:	'e.g. Adenocarcinoma, Squamous cell carcinoma (suggest limit to just these two types - although watch out for complete responders)'	,
      signet_ring_cells:	'Y/N for presence of signet ring cells in the resection histology'	,
      grade:	'Differentiation - Well, Moderate, Poor, undifferentiated'	,
      resection:	'R0 (no tumour within 1mm of any margin), R1 (tumour at (microscopically) or within 1mm of any margin), R2 tumour macroscopically at a margin = palliative resection'	,
      lymphovascalar_invasion:	'Y/N'	,
      perineural_invasion:	'Y/N'	,
      venous_invasion:	'Y/N'	,
      tnm_t:	'Using TNM 7 the t stage'	,
      tnm_n:	'Using TNM 7 the N stage'	,
      tnm_m:	'Using TNM 7 the M stage'	,
      histological_response:	'Using Mandard criteria TRG I - V. (Assessed by a consultant pathologist)'	,
      barretts_adjacent_to_tumour:	'from microscopic assessment. Including presence of dysplasia'	,

    }

    follow_up = {
      censor_value:	'Dead/Alive'	,
      dod:	'Date of death (often recorded in TRAK)'	,
      dls:	'Date last seen (in clinic, having a test at hospital or at GP, even if just to pick up a prescription)'	,
      survival_months:	'Calculated from date of diagnosis to date last seen or date of death.'	,
      recurrence_free_survival:	'Y/N/NA'	,
      date_of_recurrence:	'Date of scan or clinical appointment demonstrating suspicion of recurrence (later proven)'	,
      region_of_recurrence:	'Data from scan or post-mortem'	,
      local_recurrence:	'Local (at the previous site of the cancer - mediastinal or anastomosis), Distant (neck nodes, liver, lungs etc)'	,
      evidence_for_recurrence:	'Clinical, CT, Other Radiological, Pathological'	,
      recurrence_free_survival:	'Need to reassess all recurrence dates prior to 2010 - prone to errors.'	,

    }

    FieldDefinition.delete_all
    demographic.each_key do |key|
      FieldDefinition.create! table_name: :demographic, field_name: key, definition: demographic[key]
    end
    exposure.each_key do |key|
      FieldDefinition.create! table_name: :exposure, field_name: key, definition: exposure[key]
    end
    staging.each_key do |key|
      FieldDefinition.create! table_name: :staging, field_name: key, definition: staging[key]
    end
    laparoscopy.each_key do |key|
      FieldDefinition.create! table_name: :laparoscopy, field_name: key, definition: laparoscopy[key]
    end
    treatment.each_key do |key|
      FieldDefinition.create! table_name: :treatment, field_name: key, definition: treatment[key]
    end
    pathology.each_key do |key|
      FieldDefinition.create! table_name: :pathology, field_name: key, definition: pathology[key]
    end
    follow_up.each_key do |key|
      FieldDefinition.create! table_name: :follow_up, field_name: key, definition: follow_up[key]
    end

  end
end
