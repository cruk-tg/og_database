require 'lorem'

When /^I add a report to "(.*?)"$/ do |section|
  page
    .find(:xpath, "//h3[text()[normalize-space(.)='#{section}']]/following-sibling::div[1]/textarea")
    .set(Lorem::Base.new(:paragraphs, 1).output)
end

When /^I add the stage (T\d)(N\d)(M\d) to "(.*?)"$/ do |t,n,m,section|
  within(:xpath, "//h3[text()[normalize-space(.)='#{section}']]/following-sibling::div[@class='well'][1]") do
    all(:css, '.form-control').each do |element|
      element.find('select option:nth-child(2)').select_option
    end
  end
end
