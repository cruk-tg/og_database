When /supply some (.*?)$/ do |type|
  if type == "medications"
    within_fieldset("Medications") do
      click_link "Add"
      b_select "PPI", :from => "Drug"
      b_select "<6 months", :from => "Length of use"
    end
  elsif type == "complaints"
    b_check_box_group "Other", :from => "Presenting Complaint"
  elsif type == "co-morbidities"
   b_check_box_group "Cardiovascular", :from => "Co-Morbidity"
  elsif type == "family history"
    @history_type = "og"
    within_fieldset("Family History of Oesophageal Cancer") do
      click_link "Add"
      b_select "Father", :from => "Relationship"
    end
  elsif type == "family history other"
    @history_type = "#{@history_type}ot"
    within_fieldset("Family History of Other Cancers") do
      click_link "Add"
      b_select "Father", :from => "Relationship"
    end
  else
    pending
  end
end

Then("the medications") do
  expect(Medication.all).not_to be_empty
end

Then("the complaints") do
  expect(PresentingComplaint.all).not_to be_empty
end

Then("the co-morbidities") do
  expect(CoMorbidity.all).not_to be_empty
end

Then("the family history") do
  if @history_type.include? "og"
    expect(FamilyHistoryOfOgCancer.all).not_to be_empty
  end
  if @history_type.include? "ot"
    expect(FamilyHistoryOfOtherCancer.all).not_to be_empty
  end
end
