Given /samples exist$/ do
  demographic = Demographic.first
  FactoryBot.create :sample, demographic: demographic
end

Then /^I should have a list of samples$/ do
  within('#sample_list_table') do
    expect(page).to have_css('td')
  end
end

When /^I edit a sample$/ do
  click_button 'Edit'
  b_fill_in 'Histology text', with: 'blah'
end

Then /^I should see the updated sample$/ do
  expect(Sample.first.histology_text).to eq('blah')
end
