Given /^a (.*?) exists$/ do |model|
  @model = FactoryBot.create(model)
end

Given("I am on it's page") do
  visit url_for(controller: @model.model_name.plural,
                action: 'edit',
                id: @model.id,
                only_path: true)
end

When /^I fill in valid demographic details$/ do
  b_fill_in "CHI", :with => "1212120004"
  b_fill_in "OCCAMS ID", :with => "OCCAMS/ED/002"
  b_fill_in "OE Number", :with => "OE000005"
  b_fill_in "ECMC", :with => "24-01-2012"
  b_fill_in "OCCAMS", :with => "24-01-2012"
  b_select  "Male", :from => "Gender"
end

When("I fill in valid {string} demographic details") do |string|
  b_fill_in "CHI", :with => "1212120004"
  b_fill_in "OCCAMS ID", :with => "OCCAMS/ED/002"
  b_fill_in "ECMC", :with => "24-01-2012"
  b_fill_in "OCCAMS", :with => "24-01-2012"
  b_select  "Male", :from => "Gender"
  b_fill_in "OE Number", :with => "OC000005"
end

Then /valid demographic identifiers$/ do
  find_field("OCCAMS ID").value.should eq("OCCAMS/ED/002")
  find_field("OE Number").value.should eq("OE000005")
end

Then("the demographic table should be updated") do
  expect(Demographic.first.occams_id).to eq("OCCAMS/ED/002")
  expect(Demographic.first.gender.name).to eq("Male")
end
