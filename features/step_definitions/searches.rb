Given /^patients exist$/ do
  FactoryBot.create_list(:demographic, 5)
  FactoryBot.create_list(:demographic_postop, 10)
  FactoryBot.create_list(:demographic_recurrence_free, 20)
end

Then /^I should have patients with no recurrence$/ do
  select '100', from: 'search_results_table_length'
  within(:css, '#search_results_table') do
    all(:xpath, './/tbody/tr/td[4]').count.should eq(25)
  end
end

Then("I should have patients with only pre-op") do
  within(:css, '#search_results_table') do
    all(:xpath, './/tbody/tr/td[4]').count.should eq(5)
  end
end

Then("I should have patients with post-op records") do
  within(:css, '#search_results_table') do
    all(:xpath, './/tbody/tr/td[4]').count.should eq(10)
  end
end
