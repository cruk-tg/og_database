When("I full in evidence for recurrence") do
  b_check_box_group "CT", :from => "Evidence for recurrence"
end

Then("I should have evidence for recurrence") do
  expect(FollowUp.first.evidence_for_recurrence_ids).to_not be_empty
end
