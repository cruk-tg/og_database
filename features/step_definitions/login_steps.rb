Given("that we are logged in") do
  User.create! username: 'test_user'
  visit '/fake_cas_login'
  fill_in "Username", :with => 'test_user'
  click_button 'Login'
end

Then("a session is created") do
  redis = Redis.new
  expect(redis.keys("rack_cas*")).not_to be_empty
end
