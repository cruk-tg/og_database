Then /^I should have (\d+) "(.*?)"$/ do |arg1, model|
  @model = model.classify.constantize.first
  expect(@model).to_not be_nil
end


Then /^The field "(.*?)" has the value "(.*?)"$/ do |field,value|
  expect(@model.public_send(field.underscore).to_s).to eq(value)
end
