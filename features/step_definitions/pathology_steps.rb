When("I fill in a adjacent information") do
  b_check_box_group "Microscopic gastric metaplasia", :from => "Barrett's adjacent"
end

Then("I should have adjacent information") do
  expect(Pathology.first.barretts_adjacent_lookups).to_not be_empty
end
