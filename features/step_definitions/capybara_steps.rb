Given /^I am on the (\w+) (\w+) page$/ do |method,page|
  if page =~ /demographic[s]?/
    visit "/#{page.pluralize}/#{method}"
  else
    if method == 'edit'
      visit url_for(action: method, controller: page.pluralize, only_path: true, id: page.camelize.constantize.first.id)
    else
      visit "/demographics/#{Demographic.all.first.id}/#{page.pluralize}/#{method}"
    end

  end
end

Given /^I am on the (\w+) page$/ do |page|
  if page =~ /demographic[s]?/
    visit "/demographics/"
  else
    visit "/demographics/#{Demographic.all.first.id}/#{page.pluralize}"
  end
end

When /^I fill in "(.*?)" with "(.*?)"$/ do |element, text|
  b_fill_in element, :with => text
end

When /^I (press|click) "(.*?)"$/ do |action,name|
  click_on name
end

When /^I select "(.*?)" from "(.*?)"$/ do |value, field|
  b_select(value, :from => field)
end

Then /^I should see "(.*?)"$/ do |content|
  page.should have_content(content)
end
