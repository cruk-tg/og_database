Feature: Follow-up
  In order to know how a patient has responded to treatment
  As a oncologist
  I need to follow the patient up

  Background:
    Given that we are logged in
    And a demographic exists

  Scenario: Patient alive with no recurrence
    Given I am on the new follow_up page 
    When I select "Alive" from "Alive/Dead?"
    And I fill in "Last Follow-up" with "12-06-2012"
    And I press "Create Follow Up"
    Then I should see "Followup was created successfully"

  Scenario: Update Evidence for recurrences
    Given a follow_up exists
    And I am on the edit follow_up page
    When I full in evidence for recurrence
    And I press "Update Follow Up"
    Then I should see "Followup was updated successfully"
    And I should have evidence for recurrence
