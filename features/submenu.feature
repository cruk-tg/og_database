@javascript
Feature: Sub menu
  Missing submenu in new screens
  Issue #14

  Background:
    Given that we are logged in
    And a demographic exists

  Scenario: New Exposure
    Given I am on the new exposure page
    Then the submenu exists

  Scenario: New Staging
    Given I am on the new staging page
    Then the submenu exists

  Scenario: New Pathology
    Given I am on the new pathology page
    Then the submenu exists

  Scenario: New Follow Up
    Given I am on the new follow_up page
    Then the submenu exists

  Scenario: New Treatment
    Given I am on the new treatment page
    Then the submenu exists
