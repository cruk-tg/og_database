Feature: Stagings
  In order to assess patients current clinical status
  As an oncologies
  I want to know their staging and the information reguarding how the staging was arrived at

  Background:
    Given that we are logged in
    And a demographic exists

  Scenario: Patient with CT Staging
    Given a demographic exists
    And I am on the new staging page
    When I fill in "Date of diagnosis" with "14-02-2012"
    And I add a report to "CT Staging"
    And I add the stage T2N1M0 to "CT Staging"
    And I press "Create Staging"
    Then I should see "Staging was created successfully"

  Scenario: Patient with Endoscopic Ultrasound Staging
    Given a demographic exists
    And I am on the new staging page
    When I fill in "Date of diagnosis" with "14-02-2012"
    And I add a report to "Endoscopic Ultrasound"
    And I add the stage T2N1M0 to "Endoscopic Ultrasound"
    And I press "Create Staging"
    Then I should see "Staging was created successfully"

  Scenario: Patient With PET-CT Staging
    Given a demographic exists
    And I am on the new staging page
    When I fill in "Date of diagnosis" with "14-02-2012"
    And I add a report to "PET-CT Staging"
    And I add the stage T2N1M0 to "PET-CT Staging"
    And I press "Create Staging"
    Then I should see "Staging was created successfully"

  Scenario: Patient With Other Investigation Staging
    Given a demographic exists
    And I am on the new staging page
    When I fill in "Date of diagnosis" with "14-02-2012"
    And I add a report to "Other Investigation"
    And I press "Create Staging"
    Then I should see "Staging was created successfully"
