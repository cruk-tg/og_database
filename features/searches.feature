@javascript
Feature: Searches
  In order to find group of patients to study
  As a researcher
  I want to be able to search for patient groups

  Background:
    Given that we are logged in

  Scenario: Find all patients with pre-op
    Given patients exist
    And I am on the search demographics page
    When I click "Pre-Op"
    Then I should have patients with only pre-op

  Scenario: Find all post-ops
    Given patients exist
    And I am on the search demographics page
    When I click "Post-Op"
    Then I should have patients with post-op records

  Scenario: Find all patient without recurrence
    Given patients exist
    And I am on the search demographics page
    When I click "Recurrence Free"
    Then I should have patients with no recurrence
