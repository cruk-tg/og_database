Feature: Treatments
  In order to assess a patient
  As a researcher
  I need to know what treatment they received

  Background:
    Given that we are logged in
    And a demographic exists

  Scenario: Patient for curative treatment
    Given I am on the new treatment page
    When I select "Curative" from "Treatment intent" 
    And I select "Curative" from "Intent"
    And I press "Create Treatment"
    Then I should see "Treatment was created successfully"
