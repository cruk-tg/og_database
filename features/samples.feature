Feature: Samples
  In order to track samples in a collected
  As a research
  I want to be able to see what samples ECMC and update my information on them

  Background:
    Given that we are logged in
    And a demographic exists


  Scenario: List samples for a patient
    Given samples exist
    And I am on the samples page
    Then I should have a list of samples

  Scenario: Update sample
    Given samples exist
    And I am on the samples page
    When I edit a sample
    And I press "Update Sample"
    Then I should see "Sample was updated successfully"
    And I should see the updated sample

