Feature: Pathology
  In order to know what type of OG cancer a patient has
  As a research
  I need to know their pathology

  Background:
    Given that we are logged in
    And a demographic exists


  Scenario: Patient with low grade OG Cancer
    Given I am on the new pathology page
    When I fill in "Diagnostic Pathology number" with "UB1234567/12"
    And I select "Oesophagus upper 1/3" from "Tumour location"
    And I select "Adenocarcinoma" from "Histology"
    And I select "Well" from "Grade"
    And I press "Create Pathology"
    Then I should see "Pathology was created successfully"

  Scenario: Barrett's adjacent update
    Given a pathology exists
    And I am on the edit pathology page
    When I fill in a adjacent information
    And I press "Update Pathology"
    Then I should see "Pathology was updated successfully"
    And I should have adjacent information
