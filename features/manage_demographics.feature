Feature: Manage Demographics
  In order to study Oesophageal cancer
  As a researcher
  I want to register and manage patients

  Background:
    Given that we are logged in

  Scenario: Add Patient
    Given I am on the new demographic page
    When I fill in valid demographic details
    And I press "Create Demographic"
    Then I should see "Demographic was created successfully"
    And I should have 1 "demographic"

  Scenario: Edit Patient
    Given a demographic exists
    And I am on it's page
    When I fill in valid demographic details
    And I press "Update Demographic"
    Then I should see "Demographic was updated successfully"
    And the demographic table should be updated
