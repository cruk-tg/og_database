module ECMC
  module BootstrapHelpers
    def self.included(base)

    end

    def b_fill_in(locator, options={})
      raise "Must pass a hash containing 'with'" if not options.is_a?(Hash) or not options.has_key?(:with)
      with = options.delete(:with)
      fill_options = options.delete(:fill_options)
      b_find_control(:input, locator).set(with,fill_options)
    end

    def b_select(value, options={})
      raise "Must pass a hash containing 'from'" if not options.has_key?(:from)
      from = options.delete(:from)
      b_find_control(:select, from).find(:option, value, options).select_option
    end

    def b_check_box_group(value, options={})
      raise "Must pass a hash containing 'from'" if not options.has_key?(:from)
      from = options.delete(:from)
      all(:css, 'div.check-box-group').each do |element|
        if element.find('h3').text == from
          element.find('label', text: value).click
        end
      end
    end

    def b_find_control(tag, locator)
      all(:css, 'span.input-group-addon').each do |element|
        if element.text == locator
          return find("#{tag}[aria-describedby='#{element[:id]}']")
        end
      end
      raise "Field: #{locator} not found"
    end
  end
end
