Feature: New OC demographic
  This tests issue #17

  Background:
    Given that we are logged in

  Scenario: Add Patient
    Given I am on the new demographic page
    When I fill in valid "oc" demographic details
    And I press "Create Demographic"
    Then I should see "Demographic was created successfully"
    And I should have 1 "demographic"
