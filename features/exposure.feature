@javascript
Feature: Exposures
  In order to gain insight to the history of a patient
  As a research
  I want to know their exposures

  Background:
    Given that we are logged in
    And a demographic exists

  Scenario: Patient BMI
    Given I am on the new exposure page
    When I fill in "Height (m)" with "1.95"
    And I fill in "Current Weight (kg)" with "95"
    And I press "Create Exposure"
    Then I should see "Exposure was created successfully"
    And I should have 1 "exposure"
    And The field "BMI" has the value "24.98"

  Scenario: Patient weight loss
    Given I am on the new exposure page
    When I fill in "Height (m)" with "1.95"
    And I fill in "Current Weight (kg)" with "95"
    And I fill in "Weight Lost (kg)" with "9.0"
    And I fill in "Weight Lost over (Weeks)" with "4kg over a week"
    And I press "Create Exposure"
    Then I should see "Exposure was created successfully"
    And I should have 1 "exposure"
    And The field "weight_loss" has the value "9.0"
    And The field "weight_loss_weeks" has the value "4kg over a week"

  Scenario: Patient with medication
    Given I am on the new exposure page
    When I supply some medications
    And I press "Create Exposure"
    Then I should see "Exposure was created successfully"
    And the medications

  Scenario: Patient with family history
    Given I am on the new exposure page
    When I supply some family history
    And I press "Create Exposure"
    Then I should see "Exposure was created successfully"
    And the family history

  Scenario: Patient with family history other
    Given I am on the new exposure page
    When I supply some family history other
    And I press "Create Exposure"
    Then I should see "Exposure was created successfully"
    And the family history

  Scenario: Patient with family history and other history
    Given I am on the new exposure page
    When I supply some family history
    And I supply some family history other
    And I press "Create Exposure"
    Then I should see "Exposure was created successfully"
    And the family history

  Scenario: Patient with presenting complaints
    Given I am on the new exposure page
    When I supply some complaints
    And I press "Create Exposure"
    Then I should see "Exposure was created successfully"
    And the complaints

  Scenario: Patient with co-morbidities
    Given I am on the new exposure page
    When I supply some co-morbidities
    And I press "Create Exposure"
    Then I should see "Exposure was created successfully"
    And the co-morbidities

  Scenario: Patient FEV1
    Given I am on the new exposure page
    When I fill in "FEV1 (l/min)" with "1.95"
    And I press "Create Exposure"
    Then I should see "Exposure was created successfully"
    And I should have 1 "exposure"
    And The field "pulmonary_function_fev1_lm" has the value "1.95"
    
  Scenario: Update Co-morbidities
    Given a exposure exists
    And I am on the edit exposure page
    When I supply some co-morbidities
    And I press "Update Exposure"
    Then I should see "Exposure was updated successfully"
    And the co-morbidities

  Scenario: Update presenting complaints
    Given a exposure exists
    And I am on the edit exposure page
    When I supply some complaints
    And I press "Update Exposure"
    Then I should see "Exposure was updated successfully"
    And the complaints
    
    
