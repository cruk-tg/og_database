FROM ruby:2.4
MAINTAINER paul.d.mitchell@ed.ac.uk

ENV INSTALL_DIR="/opt/rails_app" \
    RAILS_ENV=production \
    NODE_ENV=production

RUN buildDeps='gcc libmagickcore-dev libmagickwand-dev default-libmysqlclient-dev make patch' \
    && apt-get update && apt-get install apt-transport-https \
    && wget --quiet -O - https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - \
    && echo 'deb https://deb.nodesource.com/node_8.x trusty main' > /etc/apt/sources.list.d/nodesource.list \
    && set -ex \
    && apt-get update && apt-get install -y $buildDeps nodejs --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir -p $INSTALL_DIR
WORKDIR $INSTALL_DIR

COPY Gemfile $INSTALL_DIR/Gemfile
COPY Gemfile.lock $INSTALL_DIR/Gemfile.lock

RUN bundle install \
    && apt-get purge -y --auto-remove $buildDeps

COPY . /opt/rails_app

ENTRYPOINT ["bundle", "exec"]

CMD ["rake", "ecmc:start"]
